<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bg">
    <extra-po-header-language>bg</extra-po-header-language>
    <extra-po-header-language_team>Bulgarian &lt;kde-i18n-doc@kde.org&gt;</extra-po-header-language_team>
    <extra-po-header-last_translator>Mincho Kondarev &lt;mkondarev@yahoo.de&gt;</extra-po-header-last_translator>
    <extra-po-header-po_revision_date>2024-04-06 12:34+0200</extra-po-header-po_revision_date>
    <extra-po-header-pot_creation_date></extra-po-header-pot_creation_date>
    <extra-po-header-project_id_version></extra-po-header-project_id_version>
    <extra-po-header-x_generator>Lokalize 24.02.1</extra-po-header-x_generator>
    <extra-po-header_comment># SPDX-FileCopyrightText: 2024 Mincho Kondarev &lt;mkondarev@yahoo.de&gt;</extra-po-header_comment>
    <extra-po-headers>Project-Id-Version,POT-Creation-Date,PO-Revision-Date,Last-Translator,Language-Team,Language,MIME-Version,Content-Type,Content-Transfer-Encoding,Plural-Forms,X-Language,X-Qt-Contexts,X-Generator</extra-po-headers>
<context>
    <name>BootInfoWidget</name>
    <message>
        <location filename="../src/modules/partition/gui/BootInfoWidget.cpp" line="62"/>
        <source>The &lt;strong&gt;boot environment&lt;/strong&gt; of this system.&lt;br&gt;&lt;br&gt;Older x86 systems only support &lt;strong&gt;BIOS&lt;/strong&gt;.&lt;br&gt;Modern systems usually use &lt;strong&gt;EFI&lt;/strong&gt;, but may also show up as BIOS if started in compatibility mode.</source>
        <translation>&lt;strong&gt;Средата за начално зареждане&lt;/strong&gt; на тази система.&lt;br&gt;&lt;br&gt;Старите x86 системи поддържат само &lt;strong&gt;BIOS&lt;/strong&gt;.&lt;br&gt;Модерните системи обикновено използват &lt;strong&gt;EFI&lt;/strong&gt;, но може също така да използват BIOS, ако са стартирани в режим на съвместимост.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/BootInfoWidget.cpp" line="72"/>
        <source>This system was started with an &lt;strong&gt;EFI&lt;/strong&gt; boot environment.&lt;br&gt;&lt;br&gt;To configure startup from an EFI environment, this installer must deploy a boot loader application, like &lt;strong&gt;GRUB&lt;/strong&gt; or &lt;strong&gt;systemd-boot&lt;/strong&gt; on an &lt;strong&gt;EFI System Partition&lt;/strong&gt;. This is automatic, unless you choose manual partitioning, in which case you must choose it or create it on your own.</source>
        <translation>Тази система беше стартирана с &lt;strong&gt;EFI&lt;/strong&gt; среда за начално зареждане.&lt;br&gt;&lt;br&gt;За да се настрои стартирането от EFI, инсталаторът трябва да разположи програма за начално зареждане като &lt;strong&gt;GRUB&lt;/strong&gt; или &lt;strong&gt;systemd-boot&lt;/strong&gt; на &lt;strong&gt;EFI Системен Дял&lt;/strong&gt;. Това се прави автоматично, освен ако не се избере ръчно поделяне, в такъв случай вие трябва да свършите тази работа.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/BootInfoWidget.cpp" line="84"/>
        <source>This system was started with a &lt;strong&gt;BIOS&lt;/strong&gt; boot environment.&lt;br&gt;&lt;br&gt;To configure startup from a BIOS environment, this installer must install a boot loader, like &lt;strong&gt;GRUB&lt;/strong&gt;, either at the beginning of a partition or on the &lt;strong&gt;Master Boot Record&lt;/strong&gt; near the beginning of the partition table (preferred). This is automatic, unless you choose manual partitioning, in which case you must set it up on your own.</source>
        <translation>Тази система беше стартирана с &lt;strong&gt;BIOS&lt;/strong&gt; среда за начално зареждане.&lt;br&gt;&lt;br&gt;За да се настрои стартирането от BIOS, инсталаторът трябва да разположи програма за начално зареждане като &lt;strong&gt;GRUB&lt;/strong&gt; в началото на дяла или на &lt;strong&gt;Сектора за Начално Зареждане&lt;/strong&gt; близо до началото на таблицата на дяловете (предпочитано). Това се прави автоматично, освен ако не се избере ръчно поделяне, в такъв случай вие трябва да свършите тази работа.</translation>
    </message>
</context>
<context>
    <name>BootLoaderModel</name>
    <message>
        <location filename="../src/modules/partition/core/BootLoaderModel.cpp" line="58"/>
        <source>Master Boot Record of %1</source>
        <translation>Сектор за начално зареждане на %1</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/BootLoaderModel.cpp" line="91"/>
        <source>Boot Partition</source>
        <translation>Дял за начално зареждане</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/BootLoaderModel.cpp" line="98"/>
        <source>System Partition</source>
        <translation>Системен дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/BootLoaderModel.cpp" line="128"/>
        <source>Do not install a boot loader</source>
        <translation>Без инсталиране на програма за начално зареждане</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/BootLoaderModel.cpp" line="146"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>Calamares::BlankViewStep</name>
    <message>
        <location filename="../src/libcalamaresui/viewpages/BlankViewStep.cpp" line="61"/>
        <source>Blank Page</source>
        <translation>Празна страница</translation>
    </message>
</context>
<context>
    <name>Calamares::DebugWindow</name>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="28"/>
        <source>GlobalStorage</source>
        <translation>Глобално съхранение</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="38"/>
        <source>JobQueue</source>
        <translation>Опашка от задачи</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="48"/>
        <source>Modules</source>
        <translation>Модули</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="61"/>
        <source>Type:</source>
        <translation>Вид:</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="68"/>
        <location filename="../src/calamares/DebugWindow.ui" line="82"/>
        <source>none</source>
        <translation>няма</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="75"/>
        <source>Interface:</source>
        <translation>Интерфейс:</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="97"/>
        <source>Tools</source>
        <translation>Инструменти</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="110"/>
        <source>Reload Stylesheet</source>
        <translation>Презареждане на таблицата със стилове</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.ui" line="117"/>
        <source>Widget Tree</source>
        <translation>Структура на уиджети</translation>
    </message>
    <message>
        <location filename="../src/calamares/DebugWindow.cpp" line="217"/>
        <source>Debug information</source>
        <translation>Информация за отстраняване на грешки</translation>
    </message>
</context>
<context>
    <name>Calamares::ExecutionViewStep</name>
    <message>
        <location filename="../src/libcalamaresui/viewpages/ExecutionViewStep.cpp" line="85"/>
        <source>Set up</source>
        <translation>Настройване</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/viewpages/ExecutionViewStep.cpp" line="85"/>
        <source>Install</source>
        <translation>Инсталиране</translation>
    </message>
</context>
<context>
    <name>Calamares::FailJob</name>
    <message>
        <location filename="../src/libcalamares/JobExample.cpp" line="29"/>
        <source>Job failed (%1)</source>
        <translation>Задачата се провали (%1)</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/JobExample.cpp" line="30"/>
        <source>Programmed job failure was explicitly requested.</source>
        <translation>Изрично е поискан отказ на програмираната задача.</translation>
    </message>
</context>
<context>
    <name>Calamares::JobThread</name>
    <message>
        <location filename="../src/libcalamares/JobQueue.cpp" line="196"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
</context>
<context>
    <name>Calamares::NamedJob</name>
    <message>
        <location filename="../src/libcalamares/JobExample.cpp" line="17"/>
        <source>Example job (%1)</source>
        <translation>Примерна задача (%1)</translation>
    </message>
</context>
<context>
    <name>Calamares::ProcessJob</name>
    <message>
        <location filename="../src/libcalamares/ProcessJob.cpp" line="43"/>
        <source>Run command &apos;%1&apos; in target system.</source>
        <translation>Изпълнение на команда &quot;%1&quot; в целевата система.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/ProcessJob.cpp" line="43"/>
        <source> Run command &apos;%1&apos;.</source>
        <translation>Изпълняване на команда &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/ProcessJob.cpp" line="50"/>
        <source>Running command %1 %2</source>
        <translation>Изпълняване на команда %1 %2</translation>
    </message>
</context>
<context>
    <name>Calamares::PythonJob</name>
    <message>
        <location filename="../src/libcalamares/PythonJob.cpp" line="192"/>
        <source>Running %1 operation.</source>
        <translation>Изпълнение на %1 операция.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonJob.cpp" line="221"/>
        <source>Bad working directory path</source>
        <translation>Невалиден път на работната директория</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonJob.cpp" line="222"/>
        <source>Working directory %1 for python job %2 is not readable.</source>
        <translation>Работна директория %1 за python задача %2 не се чете.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonJob.cpp" line="228"/>
        <source>Bad main script file</source>
        <translation>Невалиден файл на главен скрипт</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonJob.cpp" line="229"/>
        <source>Main script file %1 for python job %2 is not readable.</source>
        <translation>Файла на главен скрипт %1 за python задача %2 не се чете.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonJob.cpp" line="297"/>
        <source>Boost.Python error in job &quot;%1&quot;.</source>
        <translation>Boost.Python грешка в задача &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>Calamares::QmlViewStep</name>
    <message>
        <location filename="../src/libcalamaresui/viewpages/QmlViewStep.cpp" line="67"/>
        <source>Loading ...</source>
        <translation>Зареждане...</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/viewpages/QmlViewStep.cpp" line="88"/>
        <source>QML Step &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>QML Стъпка &lt;i&gt;%1&lt;/i&gt;.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/viewpages/QmlViewStep.cpp" line="268"/>
        <source>Loading failed.</source>
        <translation>Неуспешно зареждане.</translation>
    </message>
</context>
<context>
    <name>Calamares::RequirementsChecker</name>
    <message>
        <location filename="../src/libcalamares/modulesystem/RequirementsChecker.cpp" line="94"/>
        <source>Requirements checking for module &lt;i&gt;%1&lt;/i&gt; is complete.</source>
        <translation>Проверката на изискванията на модул &lt;i&gt;%1&lt;/i&gt; е завършена.</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/libcalamares/modulesystem/RequirementsChecker.cpp" line="115"/>
        <source>Waiting for %n module(s).</source>
        <translation>
            <numerusform>Изчакване на %n модул.</numerusform>
            <numerusform>Изчакване на %n модула.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/libcalamares/modulesystem/RequirementsChecker.cpp" line="116"/>
        <source>(%n second(s))</source>
        <translation>
            <numerusform>(%n секунда)</numerusform>
            <numerusform>(%n секунди)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/libcalamares/modulesystem/RequirementsChecker.cpp" line="121"/>
        <source>System-requirements checking is complete.</source>
        <translation>Проверката на системните изисквания е завършена.</translation>
    </message>
</context>
<context>
    <name>Calamares::ViewManager</name>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="150"/>
        <source>Setup Failed</source>
        <translation>Настройването е неуспешно</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="150"/>
        <source>Installation Failed</source>
        <translation>Неуспешна инсталация</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="151"/>
        <source>Would you like to paste the install log to the web?</source>
        <translation>Искате ли да поставите дневника за инсталиране в мрежата?</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="164"/>
        <source>Error</source>
        <translation>Грешка</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="171"/>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="518"/>
        <source>&amp;Yes</source>
        <translation>&amp;Да</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="172"/>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="519"/>
        <source>&amp;No</source>
        <translation>&amp;Не</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="178"/>
        <source>&amp;Close</source>
        <translation>&amp;Затваряне</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="189"/>
        <source>Install Log Paste URL</source>
        <translation>Инсталиране на дневник Вмъкване на URL адрес</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="192"/>
        <source>The upload was unsuccessful. No web-paste was done.</source>
        <translation>Качването беше неуспешно. Не беше направено поставяне в мрежата.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="208"/>
        <source>Calamares Initialization Failed</source>
        <translation>Инициализацията на Calamares се провали</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="209"/>
        <source>%1 can not be installed. Calamares was unable to load all of the configured modules. This is a problem with the way Calamares is being used by the distribution.</source>
        <translation>%1 не може да се инсталира. Calamares не можа да зареди всичките конфигурирани модули. Това е проблем с начина, по който Calamares е използван от дистрибуцията.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="215"/>
        <source>&lt;br/&gt;The following modules could not be loaded:</source>
        <translation>&lt;br/&gt;Следните модули не могат да се заредят:</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="327"/>
        <source>Continue with setup?</source>
        <translation>Продължаване?</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="327"/>
        <source>Continue with installation?</source>
        <translation>Да се продължи ли инсталирането?</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="329"/>
        <source>The %1 setup program is about to make changes to your disk in order to set up %2.&lt;br/&gt;&lt;strong&gt;You will not be able to undo these changes.&lt;/strong&gt;</source>
        <translation>Програмата за настройване на %1 е на път да направи промени на вашия диск, за да инсталира %2. &lt;br/&gt;&lt;strong&gt; Няма да можете да отмените тези промени.&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="332"/>
        <source>The %1 installer is about to make changes to your disk in order to install %2.&lt;br/&gt;&lt;strong&gt;You will not be able to undo these changes.&lt;/strong&gt;</source>
        <translation>Инсталаторът на %1 ще направи промени по вашия диск за да инсталира %2. &lt;br&gt;&lt;strong&gt;Промените ще бъдат окончателни.&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="335"/>
        <source>&amp;Set up now</source>
        <translation>&amp; Настройване сега</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="335"/>
        <source>&amp;Install now</source>
        <translation>&amp;Инсталиране сега</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="343"/>
        <source>Go &amp;back</source>
        <translation>В&amp;ръщане</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="392"/>
        <source>&amp;Set up</source>
        <translation>&amp;Настройване</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="392"/>
        <source>&amp;Install</source>
        <translation>&amp;Инсталиране</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="394"/>
        <source>Setup is complete. Close the setup program.</source>
        <translation>Настройката е завършена. Затворете програмата за настройка.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="395"/>
        <source>The installation is complete. Close the installer.</source>
        <translation>Инсталацията е завършена. Затворете инсталаторa.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="397"/>
        <source>Cancel setup without changing the system.</source>
        <translation>Отмяна на настройването без промяна на системата.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="398"/>
        <source>Cancel installation without changing the system.</source>
        <translation>Отказ от инсталацията без промяна на системата.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="408"/>
        <source>&amp;Next</source>
        <translation>&amp;Напред</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="413"/>
        <source>&amp;Back</source>
        <translation>&amp;Назад</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="419"/>
        <source>&amp;Done</source>
        <translation>&amp;Готово</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="438"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отказ</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="511"/>
        <source>Cancel setup?</source>
        <translation>Отмяна на настройването?</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="511"/>
        <source>Cancel installation?</source>
        <translation>Отмяна на инсталацията?</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="512"/>
        <source>Do you really want to cancel the current setup process?
The setup program will quit and all changes will be lost.</source>
        <translation>Наистина ли искате да анулирате текущия процес на настройване? 
Инсталирането ще се отмени и всички промени ще бъдат загубени.</translation>
    </message>
    <message>
        <location filename="../src/libcalamaresui/ViewManager.cpp" line="514"/>
        <source>Do you really want to cancel the current install process?
The installer will quit and all changes will be lost.</source>
        <translation>Наистина ли искате да отмените текущият процес на инсталиране?
Инсталаторът ще прекъсне и всичките промени ще бъдат загубени.</translation>
    </message>
</context>
<context>
    <name>CalamaresPython::Helper</name>
    <message>
        <location filename="../src/libcalamares/PythonHelper.cpp" line="288"/>
        <source>Unknown exception type</source>
        <translation>Неизвестен тип изключение</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonHelper.cpp" line="306"/>
        <source>unparseable Python error</source>
        <translation>неанализируема грешка на Python</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonHelper.cpp" line="350"/>
        <source>unparseable Python traceback</source>
        <translation>неанализируемо проследяване на Python</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/PythonHelper.cpp" line="357"/>
        <source>Unfetchable Python error.</source>
        <translation>Недостъпна грешка на Python.</translation>
    </message>
</context>
<context>
    <name>CalamaresUtils</name>
    <message>
        <location filename="../src/libcalamaresui/utils/Paste.cpp" line="25"/>
        <source>Install log posted to:
%1</source>
        <translation>Дневникът на инсталирането е записан в
%1</translation>
    </message>
</context>
<context>
    <name>CalamaresWindow</name>
    <message>
        <location filename="../src/calamares/CalamaresWindow.cpp" line="101"/>
        <source>Show debug information</source>
        <translation>Показване на информация за отстраняване на грешки</translation>
    </message>
    <message>
        <location filename="../src/calamares/CalamaresWindow.cpp" line="155"/>
        <source>&amp;Back</source>
        <translation>&amp;Назад</translation>
    </message>
    <message>
        <location filename="../src/calamares/CalamaresWindow.cpp" line="167"/>
        <source>&amp;Next</source>
        <translation>&amp;Напред</translation>
    </message>
    <message>
        <location filename="../src/calamares/CalamaresWindow.cpp" line="180"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отказ</translation>
    </message>
    <message>
        <location filename="../src/calamares/CalamaresWindow.cpp" line="304"/>
        <source>%1 Setup Program</source>
        <translation>%1 програма за настройка</translation>
    </message>
    <message>
        <location filename="../src/calamares/CalamaresWindow.cpp" line="305"/>
        <source>%1 Installer</source>
        <translation>%1 Инсталатор</translation>
    </message>
</context>
<context>
    <name>CheckerContainer</name>
    <message>
        <location filename="../src/modules/welcome/checker/CheckerContainer.cpp" line="37"/>
        <source>Gathering system information...</source>
        <translation>Събиране на системна информация...</translation>
    </message>
</context>
<context>
    <name>ChoicePage</name>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="120"/>
        <source>Select storage de&amp;vice:</source>
        <translation>Изберете ус&amp;тройство за съхранение:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="121"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="957"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1002"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1092"/>
        <source>Current:</source>
        <translation>Сегашен:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="122"/>
        <source>After:</source>
        <translation>След:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="299"/>
        <source>&lt;strong&gt;Manual partitioning&lt;/strong&gt;&lt;br/&gt;You can create or resize partitions yourself.</source>
        <translation>&lt;strong&gt;Самостоятелно поделяне&lt;/strong&gt;&lt;br/&gt;Можете да създадете или преоразмерите дяловете сами.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="822"/>
        <source>Reuse %1 as home partition for %2.</source>
        <translation>Използване на %1 като домашен дял за %2</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="958"/>
        <source>&lt;strong&gt;Select a partition to shrink, then drag the bottom bar to resize&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Изберете дял за смаляване, после влачете долната лента за преоразмеряване&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="975"/>
        <source>%1 will be shrunk to %2MiB and a new %3MiB partition will be created for %4.</source>
        <translation> %1 ще бъде намален до %2MiB и ще бъде създаден нов %3MiB дял за %4.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1031"/>
        <source>Boot loader location:</source>
        <translation>Местоположение на програмата за начално зареждане:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1083"/>
        <source>&lt;strong&gt;Select a partition to install on&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Изберете дял за инсталацията&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1142"/>
        <source>An EFI system partition cannot be found anywhere on this system. Please go back and use manual partitioning to set up %1.</source>
        <translation>EFI системен дял не е намерен. Моля, опитайте пак като използвате ръчно поделяне за %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1150"/>
        <source>The EFI system partition at %1 will be used for starting %2.</source>
        <translation>EFI системен дял в %1 ще бъде използван за стартиране на %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1158"/>
        <source>EFI system partition:</source>
        <translation>EFI системен дял:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1291"/>
        <source>This storage device does not seem to have an operating system on it. What would you like to do?&lt;br/&gt;You will be able to review and confirm your choices before any change is made to the storage device.</source>
        <translation>Това устройство за съхранение няма инсталирана операционна система. Какво ще правите?&lt;br/&gt;Ще може да прегледате и потвърдите избора си, преди да се направят промени по устройството за съхранение.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1296"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1333"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1355"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1380"/>
        <source>&lt;strong&gt;Erase disk&lt;/strong&gt;&lt;br/&gt;This will &lt;font color=&quot;red&quot;&gt;delete&lt;/font&gt; all data currently present on the selected storage device.</source>
        <translation>&lt;strong&gt;Изтриване на диска&lt;/strong&gt;&lt;br/&gt;Това ще &lt;font color=&quot;red&quot;&gt;изтрие&lt;/font&gt; всички данни върху устройството за съхранение.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1300"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1329"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1351"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1376"/>
        <source>&lt;strong&gt;Install alongside&lt;/strong&gt;&lt;br/&gt;The installer will shrink a partition to make room for %1.</source>
        <translation>&lt;strong&gt;Съвместно инсталиране&lt;/strong&gt;&lt;br/&gt;Инсталаторът ще раздроби дяла за да направи място за %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1304"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1338"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1359"/>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1384"/>
        <source>&lt;strong&gt;Replace a partition&lt;/strong&gt;&lt;br/&gt;Replaces a partition with %1.</source>
        <translation>&lt;strong&gt;Заменяне на дял&lt;/strong&gt;&lt;br/&gt;Заменя този дял с %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1323"/>
        <source>This storage device has %1 on it. What would you like to do?&lt;br/&gt;You will be able to review and confirm your choices before any change is made to the storage device.</source>
        <translation>Това устройство за съхранение има инсталиран %1. Какво ще правите?&lt;br/&gt;Ще може да прегледате и потвърдите избора си, преди да се направят промени по устройството за съхранение.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1346"/>
        <source>This storage device already has an operating system on it. What would you like to do?&lt;br/&gt;You will be able to review and confirm your choices before any change is made to the storage device.</source>
        <translation>Това устройство за съхранение има инсталирана операционна система. Какво ще правите?&lt;br/&gt;Ще може да прегледате и потвърдите избора си, преди да се направят промени по устройството за съхранение.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1371"/>
        <source>This storage device has multiple operating systems on it. What would you like to do?&lt;br/&gt;You will be able to review and confirm your choices before any change is made to the storage device.</source>
        <translation>Това устройство за съхранение има инсталирани операционни системи. Какво ще правите?&lt;br/&gt;Ще може да прегледате и потвърдите избора си, преди да се направят промени по устройството за съхранение.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1443"/>
        <source>This storage device already has an operating system on it, but the partition table &lt;strong&gt;%1&lt;/strong&gt; is different from the needed &lt;strong&gt;%2&lt;/strong&gt;.&lt;br/&gt;</source>
        <translation>Това устройство за съхранение вече има операционна система върху него, но таблицата с дялове &lt;strong&gt;%1 &lt;/strong&gt; е различна от необходимата &lt;strong&gt;%2 &lt;/strong&gt;.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1466"/>
        <source>This storage device has one of its partitions &lt;strong&gt;mounted&lt;/strong&gt;.</source>
        <translation>Това устройство за съхранение има &lt;strong&gt; монтиран &lt;/strong&gt; дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1471"/>
        <source>This storage device is a part of an &lt;strong&gt;inactive RAID&lt;/strong&gt; device.</source>
        <translation>Това устройство за съхранение е част от &lt;strong&gt; неактивно RAID &lt;/strong&gt; устройство.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1598"/>
        <source>No Swap</source>
        <translation>Без swap</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1606"/>
        <source>Reuse Swap</source>
        <translation>Повторно използване на swap</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1609"/>
        <source>Swap (no Hibernate)</source>
        <translation>Swap (без Хибернация)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1612"/>
        <source>Swap (with Hibernate)</source>
        <translation>Swap (с Хибернация)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ChoicePage.cpp" line="1615"/>
        <source>Swap to file</source>
        <translation>Swap във файл</translation>
    </message>
</context>
<context>
    <name>ClearMountsJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/ClearMountsJob.cpp" line="42"/>
        <source>Clear mounts for partitioning operations on %1</source>
        <translation>Разчистване на монтиранията за операциите на подялбата на %1</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ClearMountsJob.cpp" line="49"/>
        <source>Clearing mounts for partitioning operations on %1.</source>
        <translation>Разчистване на монтиранията за операциите на подялбата на %1</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ClearMountsJob.cpp" line="224"/>
        <source>Cleared all mounts for %1</source>
        <translation>Разчистени са всички монтирания за %1</translation>
    </message>
</context>
<context>
    <name>ClearTempMountsJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/ClearTempMountsJob.cpp" line="32"/>
        <source>Clear all temporary mounts.</source>
        <translation>Разчистване на всички временни монтирания.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ClearTempMountsJob.cpp" line="39"/>
        <source>Clearing all temporary mounts.</source>
        <translation>Разчистване на всички временни монтирания.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ClearTempMountsJob.cpp" line="51"/>
        <source>Cannot get list of temporary mounts.</source>
        <translation>Не може да се получи списък на временни монтирания.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ClearTempMountsJob.cpp" line="92"/>
        <source>Cleared all temporary mounts.</source>
        <translation>Разчистени са всички временни монтирания.</translation>
    </message>
</context>
<context>
    <name>CommandList</name>
    <message>
        <location filename="../src/libcalamares/utils/CommandList.cpp" line="142"/>
        <location filename="../src/libcalamares/utils/CommandList.cpp" line="155"/>
        <source>Could not run command.</source>
        <translation>Командата не може да се изпълни.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CommandList.cpp" line="143"/>
        <source>The command runs in the host environment and needs to know the root path, but no rootMountPoint is defined.</source>
        <translation>Командата се изпълнява в средата на хоста и трябва да установи местоположението на основния дял, но rootMountPoint не е определен.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CommandList.cpp" line="156"/>
        <source>The command needs to know the user&apos;s name, but no username is defined.</source>
        <translation>Командата трябва да установи потребителското име на профила, но такова не е определено.</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <location filename="../src/modules/keyboard/Config.cpp" line="326"/>
        <source>Set keyboard model to %1.&lt;br/&gt;</source>
        <translation>Задаване на модел на клавиатурата на %1.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/Config.cpp" line="333"/>
        <source>Set keyboard layout to %1/%2.</source>
        <translation>Задаване на подредбата на клавиатурата на %1/%2.</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/Config.cpp" line="334"/>
        <source>Set timezone to %1/%2.</source>
        <translation>Задаване на часовата зона на %1/%2.</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/Config.cpp" line="372"/>
        <source>The system language will be set to %1.</source>
        <translation>Системният език ще бъде %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/Config.cpp" line="379"/>
        <source>The numbers and dates locale will be set to %1.</source>
        <translation>Форматът на цифрите и датата ще бъде %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/Config.cpp" line="38"/>
        <source>Network Installation. (Disabled: Incorrect configuration)</source>
        <translation>Мрежова инсталация. (Деактивирано: Неправилна конфигурация)</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/Config.cpp" line="40"/>
        <source>Network Installation. (Disabled: Received invalid groups data)</source>
        <translation>Мрежова инсталация. (Изключена: Получени са данни за невалидни групи)</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/Config.cpp" line="42"/>
        <source>Network Installation. (Disabled: internal error)</source>
        <translation>Мрежова инсталация. (Изключена: вътрешна грешка)</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/Config.cpp" line="44"/>
        <source>Network Installation. (Disabled: Unable to fetch package lists, check your network connection)</source>
        <translation>Мрежова инсталация. (Изключена: Списъкът с пакети не може да бъде извлечен, проверете Вашата Интернет връзка)</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="50"/>
        <source>This computer does not satisfy the minimum requirements for setting up %1.&lt;br/&gt;Setup cannot continue. &lt;a href=&quot;#details&quot;&gt;Details...&lt;/a&gt;</source>
        <translation>Този компютър не отговаря на минималните изисквания за настройване на %1.&lt;br/&gt;Инсталацията не може да продължи. &lt;a href=&quot;#details&quot;&gt; Подробности...&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="54"/>
        <source>This computer does not satisfy the minimum requirements for installing %1.&lt;br/&gt;Installation cannot continue. &lt;a href=&quot;#details&quot;&gt;Details...&lt;/a&gt;</source>
        <translation>Този компютър не отговаря на минималните изисквания за инсталиране %1.&lt;br/&gt;Инсталацията не може да продължи.
&lt;a href=&quot;#details&quot;&gt;Детайли...&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="61"/>
        <source>This computer does not satisfy some of the recommended requirements for setting up %1.&lt;br/&gt;Setup can continue, but some features might be disabled.</source>
        <translation>Този ​​компютър не удовлетворява някои от препоръчителните изисквания за настройването на %1. &lt;br/&gt; Настройката може да продължи, но някои функции могат да бъдат деактивирани.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="65"/>
        <source>This computer does not satisfy some of the recommended requirements for installing %1.&lt;br/&gt;Installation can continue, but some features might be disabled.</source>
        <translation>Този компютър не отговаря на някои от препоръчителните изисквания за инсталиране %1.&lt;br/&gt;Инсталацията може да продължи, но някои свойства могат да бъдат недостъпни.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="75"/>
        <source>This program will ask you some questions and set up %2 on your computer.</source>
        <translation>Тази програма ще ви зададе няколко въпроса и ще конфигурира %2 на вашия компютър.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="244"/>
        <source>&lt;h1&gt;Welcome to the Calamares setup program for %1&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; Добре дошли в програмата за настройване на Calamares за %1 &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="245"/>
        <source>&lt;h1&gt;Welcome to %1 setup&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; Добре дошли в инсталатора на %1 &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="250"/>
        <source>&lt;h1&gt;Welcome to the Calamares installer for %1&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; Добре дошли в инсталатора на Calamares за %1 &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/Config.cpp" line="251"/>
        <source>&lt;h1&gt;Welcome to the %1 installer&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; Добре дошли в инсталатора %1 &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="212"/>
        <source>Your username is too long.</source>
        <translation>Вашето потребителско име е твърде дълго.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="218"/>
        <source>&apos;%1&apos; is not allowed as username.</source>
        <translation>&quot;%1&quot; не е разрешено като потребителско име.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="225"/>
        <source>Your username must start with a lowercase letter or underscore.</source>
        <translation>Вашето потребителско име трябва да започне с малки букви или долна черта.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="229"/>
        <source>Only lowercase letters, numbers, underscore and hyphen are allowed.</source>
        <translation>Разрешени са само малки букви, цифри, долна черта и тире.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="274"/>
        <source>Your hostname is too short.</source>
        <translation>Вашето име на хоста е твърде кратко.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="278"/>
        <source>Your hostname is too long.</source>
        <translation>Вашето име на хоста е твърде дълго.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="284"/>
        <source>&apos;%1&apos; is not allowed as hostname.</source>
        <translation>&quot;%1&quot; не е разрешено като име на хост. </translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="290"/>
        <source>Only letters, numbers, underscore and hyphen are allowed.</source>
        <translation>Разрешени са само букви, цифри, долна черта и тире.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="527"/>
        <source>Your passwords do not match!</source>
        <translation>Паролите Ви не съвпадат!</translation>
    </message>
</context>
<context>
    <name>ContextualProcessJob</name>
    <message>
        <location filename="../src/modules/contextualprocess/ContextualProcessJob.cpp" line="119"/>
        <source>Contextual Processes Job</source>
        <translation>Задача с контекстуални процеси</translation>
    </message>
</context>
<context>
    <name>CreatePartitionDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="18"/>
        <source>Create a Partition</source>
        <translation>Създаване на дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="42"/>
        <source>Si&amp;ze:</source>
        <translation>Раз&amp;мер:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="52"/>
        <source> MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="59"/>
        <source>Partition &amp;Type:</source>
        <translation>&amp;Тип на дяла:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="71"/>
        <source>&amp;Primary</source>
        <translation>&amp;Основен</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="81"/>
        <source>E&amp;xtended</source>
        <translation>Р&amp;азширен</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="123"/>
        <source>Fi&amp;le System:</source>
        <translation>Фа&amp;йлова система:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="155"/>
        <source>LVM LV name</source>
        <translation>LVM LV име</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="165"/>
        <source>&amp;Mount Point:</source>
        <translation>Точка на &amp;монтиране:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.ui" line="192"/>
        <source>Flags:</source>
        <translation>Флагове:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.cpp" line="66"/>
        <source>En&amp;crypt</source>
        <translation>Кри&amp;птиране</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.cpp" line="161"/>
        <source>Logical</source>
        <translation>Логически</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.cpp" line="166"/>
        <source>Primary</source>
        <translation>Главен</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.cpp" line="185"/>
        <source>GPT</source>
        <translation>GPT</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionDialog.cpp" line="265"/>
        <source>Mountpoint already in use. Please select another one.</source>
        <translation>Точката за монтиране вече се използва. Моля изберете друга.</translation>
    </message>
</context>
<context>
    <name>CreatePartitionJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionJob.cpp" line="38"/>
        <source>Create new %2MiB partition on %4 (%3) with file system %1.</source>
        <translation>Създаване на нов %2mib дял на %4 ( %3) с файлова система %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionJob.cpp" line="49"/>
        <source>Create new &lt;strong&gt;%2MiB&lt;/strong&gt; partition on &lt;strong&gt;%4&lt;/strong&gt; (%3) with file system &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Създаване на нов &lt;strong&gt;%2Mib &lt;/strong&gt; дял на &lt;strong&gt;%4 &lt;/strong&gt; (%3) с файлова система &lt;strong&gt;%1 &lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionJob.cpp" line="61"/>
        <source>Creating new %1 partition on %2.</source>
        <translation>Създаване на нов %1  дял върху %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionJob.cpp" line="73"/>
        <source>The installer failed to create partition on disk &apos;%1&apos;.</source>
        <translation>Инсталаторът не успя да създаде дял върху диск &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>CreatePartitionTableDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionTableDialog.ui" line="24"/>
        <source>Create Partition Table</source>
        <translation>Създаване на таблица на дяловете</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionTableDialog.ui" line="43"/>
        <source>Creating a new partition table will delete all existing data on the disk.</source>
        <translation>Създаването на нова таблица на дяловете ще изтрие всички съществуващи данни на диска.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionTableDialog.ui" line="69"/>
        <source>What kind of partition table do you want to create?</source>
        <translation>Какъв тип таблица на дяловете искате да създадете?</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionTableDialog.ui" line="76"/>
        <source>Master Boot Record (MBR)</source>
        <translation>Сектор за начално зареждане (MBR)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/CreatePartitionTableDialog.ui" line="86"/>
        <source>GUID Partition Table (GPT)</source>
        <translation>GUID Таблица на дяловете (GPT)</translation>
    </message>
</context>
<context>
    <name>CreatePartitionTableJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionTableJob.cpp" line="39"/>
        <source>Create new %1 partition table on %2.</source>
        <translation>Създаване на нова %1 таблица на дяловете върху %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionTableJob.cpp" line="47"/>
        <source>Create new &lt;strong&gt;%1&lt;/strong&gt; partition table on &lt;strong&gt;%2&lt;/strong&gt; (%3).</source>
        <translation>Създаване на нова &lt;strong&gt;%1&lt;/strong&gt; таблица на дяловете върху &lt;strong&gt;%2&lt;/strong&gt; (%3).</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionTableJob.cpp" line="57"/>
        <source>Creating new %1 partition table on %2.</source>
        <translation>Създаване на нова %1 таблица на дяловете върху %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreatePartitionTableJob.cpp" line="75"/>
        <source>The installer failed to create a partition table on %1.</source>
        <translation>Инсталаторът не можа да създаде таблица на дяловете върху  %1.</translation>
    </message>
</context>
<context>
    <name>CreateUserJob</name>
    <message>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="36"/>
        <source>Create user %1</source>
        <translation>Създаване на потребител %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="43"/>
        <source>Create user &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Създаване на потребител &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="126"/>
        <source>Preserving home directory</source>
        <translation>Запазване на домашната директория</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="50"/>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="143"/>
        <source>Creating user %1</source>
        <translation>Създаване на потребител %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="151"/>
        <source>Configuring user %1</source>
        <translation>Конфигуриране на потребител %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CreateUserJob.cpp" line="159"/>
        <source>Setting file permissions</source>
        <translation>Задаване на разрешения за файлове</translation>
    </message>
</context>
<context>
    <name>CreateVolumeGroupDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/CreateVolumeGroupDialog.cpp" line="28"/>
        <source>Create Volume Group</source>
        <translation>Създаване на група дялове</translation>
    </message>
</context>
<context>
    <name>CreateVolumeGroupJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/CreateVolumeGroupJob.cpp" line="31"/>
        <source>Create new volume group named %1.</source>
        <translation>Създаване на нова група дялове с име %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreateVolumeGroupJob.cpp" line="37"/>
        <source>Create new volume group named &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Създаване на нова група дялове с име  &lt;strong&gt;%1 &lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreateVolumeGroupJob.cpp" line="43"/>
        <source>Creating new volume group named %1.</source>
        <translation>Създаване на нова група дялове с име %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/CreateVolumeGroupJob.cpp" line="55"/>
        <source>The installer failed to create a volume group named &apos;%1&apos;.</source>
        <translation>Инсталаторът не успя да създаде група дялове с име „%1 “.</translation>
    </message>
</context>
<context>
    <name>DeactivateVolumeGroupJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/DeactivateVolumeGroupJob.cpp" line="24"/>
        <location filename="../src/modules/partition/jobs/DeactivateVolumeGroupJob.cpp" line="36"/>
        <source>Deactivate volume group named %1.</source>
        <translation>Деактивиране на група дялове с име  %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/DeactivateVolumeGroupJob.cpp" line="30"/>
        <source>Deactivate volume group named &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Деактивиране на група дялове с име  &lt;strong&gt;%1 &lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/DeactivateVolumeGroupJob.cpp" line="48"/>
        <source>The installer failed to deactivate a volume group named %1.</source>
        <translation>Инсталаторът не успя да деактивира група дялове с име %1.</translation>
    </message>
</context>
<context>
    <name>DeletePartitionJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/DeletePartitionJob.cpp" line="31"/>
        <source>Delete partition %1.</source>
        <translation>Изтриване на дял %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/DeletePartitionJob.cpp" line="38"/>
        <source>Delete partition &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Изтриване на дял &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/DeletePartitionJob.cpp" line="45"/>
        <source>Deleting partition %1.</source>
        <translation>Изтриване на дял %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/DeletePartitionJob.cpp" line="56"/>
        <source>The installer failed to delete partition %1.</source>
        <translation>Инсталаторът не успя да изтрие дял %1.</translation>
    </message>
</context>
<context>
    <name>DeviceInfoWidget</name>
    <message>
        <location filename="../src/modules/partition/gui/DeviceInfoWidget.cpp" line="97"/>
        <source>This device has a &lt;strong&gt;%1&lt;/strong&gt; partition table.</source>
        <translation>Устройството има &lt;strong&gt;%1&lt;/strong&gt; таблица на дяловете.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/DeviceInfoWidget.cpp" line="104"/>
        <source>This is a &lt;strong&gt;loop&lt;/strong&gt; device.&lt;br&gt;&lt;br&gt;It is a pseudo-device with no partition table that makes a file accessible as a block device. This kind of setup usually only contains a single filesystem.</source>
        <translation>Това е &lt;strong&gt;loop&lt;/strong&gt; устройство.&lt;br&gt;&lt;br&gt;Представлява псевдо-устройство, без таблица на дяловете, което прави файловете достъпни като блок устройства. Обикновено съдържа само една файлова система.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/DeviceInfoWidget.cpp" line="111"/>
        <source>This installer &lt;strong&gt;cannot detect a partition table&lt;/strong&gt; on the selected storage device.&lt;br&gt;&lt;br&gt;The device either has no partition table, or the partition table is corrupted or of an unknown type.&lt;br&gt;This installer can create a new partition table for you, either automatically, or through the manual partitioning page.</source>
        <translation>Инсталаторът &lt;strong&gt;не може да открие таблица на дяловете&lt;/strong&gt; на избраното устройство за съхранение.&lt;br&gt;&lt;br&gt;Таблицата на дяловете липсва, повредена е или е от неизвестен тип.&lt;br&gt;Инсталаторът може да създаде нова таблица на дяловете автоматично или ръчно, чрез програмата за подялба.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/DeviceInfoWidget.cpp" line="121"/>
        <source>&lt;br&gt;&lt;br&gt;This is the recommended partition table type for modern systems which start from an &lt;strong&gt;EFI&lt;/strong&gt; boot environment.</source>
        <translation>&lt;br&gt;&lt;br&gt;Това е препоръчаният тип на таблицата на дяловете за модерни системи, които стартират от &lt;strong&gt;EFI&lt;/strong&gt; среда за начално зареждане.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/DeviceInfoWidget.cpp" line="127"/>
        <source>&lt;br&gt;&lt;br&gt;This partition table type is only advisable on older systems which start from a &lt;strong&gt;BIOS&lt;/strong&gt; boot environment. GPT is recommended in most other cases.&lt;br&gt;&lt;br&gt;&lt;strong&gt;Warning:&lt;/strong&gt; the MBR partition table is an obsolete MS-DOS era standard.&lt;br&gt;Only 4 &lt;em&gt;primary&lt;/em&gt; partitions may be created, and of those 4, one can be an &lt;em&gt;extended&lt;/em&gt; partition, which may in turn contain many &lt;em&gt;logical&lt;/em&gt; partitions.</source>
        <translation>&lt;br&gt;&lt;br&gt;Тази таблица на дяловете е препоръчителна само за стари системи, които стартират с &lt;strong&gt;BIOS&lt;/strong&gt; среда за начално зареждане. GPT е препоръчителна в повечето случаи.&lt;br&gt;&lt;br&gt;&lt;strong&gt;Внимание:&lt;/strong&gt; MBR таблица на дяловете е остарял стандарт от времето на MS-DOS.&lt;br&gt;Само 4 &lt;em&gt;главни&lt;/em&gt; дяла могат да бъдат създадени и от тях само един може да е &lt;em&gt;разширен&lt;/em&gt; дял, който може да съдържа много &lt;em&gt;логически&lt;/em&gt; дялове.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/DeviceInfoWidget.cpp" line="140"/>
        <source>The type of &lt;strong&gt;partition table&lt;/strong&gt; on the selected storage device.&lt;br&gt;&lt;br&gt;The only way to change the partition table type is to erase and recreate the partition table from scratch, which destroys all data on the storage device.&lt;br&gt;This installer will keep the current partition table unless you explicitly choose otherwise.&lt;br&gt;If unsure, on modern systems GPT is preferred.</source>
        <translation>Типа на &lt;strong&gt;таблицата на дяловете&lt;/strong&gt; на избраното устройство за съхранение.&lt;br&gt;&lt;br&gt;Единствения начин да се промени е като се изчисти и пресъздаде таблицата на дяловете, като по този начин всички данни върху устройството ще бъдат унищожени.&lt;br&gt;Инсталаторът ще запази сегашната таблица на дяловете, освен ако не изберете обратното.&lt;br&gt;Ако не сте сигурни - за модерни системи се препоръчва GPT.</translation>
    </message>
</context>
<context>
    <name>DeviceModel</name>
    <message>
        <location filename="../src/modules/partition/core/DeviceModel.cpp" line="84"/>
        <source>%1 - %2 (%3)</source>
        <extracomment>device[name] - size[number] (device-node[name])</extracomment>
        <translation>%1 - %2 (%3)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/DeviceModel.cpp" line="95"/>
        <source>%1 - (%2)</source>
        <extracomment>device[name] - (device-node[name])</extracomment>
        <translation>%1 - (%2)</translation>
    </message>
</context>
<context>
    <name>DracutLuksCfgJob</name>
    <message>
        <location filename="../src/modules/dracutlukscfg/DracutLuksCfgJob.cpp" line="117"/>
        <source>Write LUKS configuration for Dracut to %1</source>
        <translation>Записване на LUKS конфигурация за Dracut на %1</translation>
    </message>
    <message>
        <location filename="../src/modules/dracutlukscfg/DracutLuksCfgJob.cpp" line="121"/>
        <source>Skip writing LUKS configuration for Dracut: &quot;/&quot; partition is not encrypted</source>
        <translation>Пропускане на записването на LUKS конфигурация за Dracut: &quot;/&quot; дялът не е криптиран</translation>
    </message>
    <message>
        <location filename="../src/modules/dracutlukscfg/DracutLuksCfgJob.cpp" line="138"/>
        <source>Failed to open %1</source>
        <translation>Неуспешно отваряне на %1</translation>
    </message>
</context>
<context>
    <name>DummyCppJob</name>
    <message>
        <location filename="../src/modules/dummycpp/DummyCppJob.cpp" line="37"/>
        <source>Dummy C++ Job</source>
        <translation>Фиктивна С++ задача</translation>
    </message>
</context>
<context>
    <name>EditExistingPartitionDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="24"/>
        <source>Edit Existing Partition</source>
        <translation>Редактиране съществуващ дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="54"/>
        <source>Content:</source>
        <translation>Съдържание:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="64"/>
        <source>&amp;Keep</source>
        <translation>&amp;Запазване</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="74"/>
        <source>Format</source>
        <translation>Форматирай</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="93"/>
        <source>Warning: Formatting the partition will erase all existing data.</source>
        <translation>Предупреждение: Форматирането на дялът ще изтрие всички съществуващи данни.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="103"/>
        <source>&amp;Mount Point:</source>
        <translation>&amp;Точка на монтиране:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="123"/>
        <source>Si&amp;ze:</source>
        <translation>Раз&amp;мер:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="133"/>
        <source> MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="140"/>
        <source>Fi&amp;le System:</source>
        <translation>Фа&amp;йлова система:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.ui" line="153"/>
        <source>Flags:</source>
        <translation>Флагове:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EditExistingPartitionDialog.cpp" line="275"/>
        <source>Mountpoint already in use. Please select another one.</source>
        <translation>Точката за монтиране вече се използва. Моля изберете друга.</translation>
    </message>
</context>
<context>
    <name>EncryptWidget</name>
    <message>
        <location filename="../src/modules/partition/gui/EncryptWidget.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EncryptWidget.ui" line="36"/>
        <source>En&amp;crypt system</source>
        <translation>Крип&amp;тиране на системата</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EncryptWidget.ui" line="46"/>
        <source>Passphrase</source>
        <translation>Парола</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EncryptWidget.ui" line="56"/>
        <source>Confirm passphrase</source>
        <translation>Потвърждаване на паролата</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/EncryptWidget.cpp" line="104"/>
        <location filename="../src/modules/partition/gui/EncryptWidget.cpp" line="114"/>
        <source>Please enter the same passphrase in both boxes.</source>
        <translation>Моля, въведете еднаква парола в двете полета.</translation>
    </message>
</context>
<context>
    <name>FillGlobalStorageJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="138"/>
        <source>Set partition information</source>
        <translation>Задаване на информация за дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="164"/>
        <source>Install %1 on &lt;strong&gt;new&lt;/strong&gt; %2 system partition.</source>
        <translation>Инсталиране на %1 на &lt;strong&gt;нов&lt;/strong&gt; %2 системен дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="170"/>
        <source>Set up &lt;strong&gt;new&lt;/strong&gt; %2 partition with mount point &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Създаване на &lt;strong&gt;нов&lt;/strong&gt; %2 дял със точка на монтиране &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="180"/>
        <source>Install %2 on %3 system partition &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Инсталиране на %2 на %3 системен дял &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="187"/>
        <source>Set up %3 partition &lt;strong&gt;%1&lt;/strong&gt; with mount point &lt;strong&gt;%2&lt;/strong&gt;.</source>
        <translation>Създаване на %3 дял &lt;strong&gt;%1&lt;/strong&gt; с точка на монтиране &lt;strong&gt;%2&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="200"/>
        <source>Install boot loader on &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Инсталиране на програма за начално зареждане върху &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FillGlobalStorageJob.cpp" line="209"/>
        <source>Setting up mount points.</source>
        <translation>Настройка на точките за монтиране.</translation>
    </message>
</context>
<context>
    <name>FinishedPage</name>
    <message>
        <location filename="../src/modules/finished/FinishedPage.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.ui" line="102"/>
        <source>&amp;Restart now</source>
        <translation>&amp;Рестартиране сега</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.cpp" line="44"/>
        <source>&lt;h1&gt;All done.&lt;/h1&gt;&lt;br/&gt;%1 has been set up on your computer.&lt;br/&gt;You may now start using your new system.</source>
        <translation>&lt;h1&gt; Всичко е готово.&lt;/h1&gt;&lt;br/&gt;%1 е инсталиран на вашия компютър. &lt;br/&gt; Сега може да започнете да използвате новата си система.</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.cpp" line="48"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this box is checked, your system will restart immediately when you click on &lt;span style=&quot;font-style:italic;&quot;&gt;Done&lt;/span&gt; or close the setup program.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Когато това поле бъде отметнато, вашата система ще се рестартира веднага , когато щракнете върху &lt;span style=&quot;font-style:italic;&quot;&gt; Готово &lt;/span&gt;или затворите програмата за инсталиране.&lt;/p&gt;&lt;/ody&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.cpp" line="54"/>
        <source>&lt;h1&gt;All done.&lt;/h1&gt;&lt;br/&gt;%1 has been installed on your computer.&lt;br/&gt;You may now restart into your new system, or continue using the %2 Live environment.</source>
        <translation>&lt;h1&gt;Всичко е завършено.&lt;/h1&gt;&lt;br/&gt;%1 беше инсталирана на вашият компютър.&lt;br/&gt;Вече можете да рестартирате в новата си система или да продължите да използвате инсталационната среда на %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.cpp" line="59"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this box is checked, your system will restart immediately when you click on &lt;span style=&quot;font-style:italic;&quot;&gt;Done&lt;/span&gt; or close the installer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Когато това поле бъде отметнато, вашата система ще се рестартира веднага, когато щракнете върху  &lt;span style=&quot;font-style:italic;&quot;&gt;Готово&lt;/span&gt; или затворете инсталатора.&lt;/p&gt;&lt;/ody&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.cpp" line="116"/>
        <source>&lt;h1&gt;Setup Failed&lt;/h1&gt;&lt;br/&gt;%1 has not been set up on your computer.&lt;br/&gt;The error message was: %2.</source>
        <translation>&lt;h1&gt; Инсталирането е неуспешно &lt;/h1&gt;&lt;br/&gt;%1 не е инсталиран на вашия компютър. &lt;br/&gt;Съобщението за грешка беше: %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedPage.cpp" line="122"/>
        <source>&lt;h1&gt;Installation Failed&lt;/h1&gt;&lt;br/&gt;%1 has not been installed on your computer.&lt;br/&gt;The error message was: %2.</source>
        <translation>&lt;h1&gt;Инсталацията е неуспешна&lt;/h1&gt;&lt;br/&gt;%1 не е инсталиран на Вашия компютър.&lt;br/&gt;Съобщението с грешката е: %2.</translation>
    </message>
</context>
<context>
    <name>FinishedViewStep</name>
    <message>
        <location filename="../src/modules/finished/FinishedViewStep.cpp" line="67"/>
        <source>Finish</source>
        <translation>Завършване</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedViewStep.cpp" line="125"/>
        <source>Setup Complete</source>
        <translation>Настройването завърши.</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedViewStep.cpp" line="125"/>
        <source>Installation Complete</source>
        <translation>Инсталацията е завършена</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedViewStep.cpp" line="127"/>
        <source>The setup of %1 is complete.</source>
        <translation>Настройката на %1 е пълна.</translation>
    </message>
    <message>
        <location filename="../src/modules/finished/FinishedViewStep.cpp" line="128"/>
        <source>The installation of %1 is complete.</source>
        <translation>Инсталацията на %1 е завършена.</translation>
    </message>
</context>
<context>
    <name>FormatPartitionJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/FormatPartitionJob.cpp" line="36"/>
        <source>Format partition %1 (file system: %2, size: %3 MiB) on %4.</source>
        <translation>Форматиране на дял %1 (файлова система: %2, размер: %3 MiB) на %4.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FormatPartitionJob.cpp" line="47"/>
        <source>Format &lt;strong&gt;%3MiB&lt;/strong&gt; partition &lt;strong&gt;%1&lt;/strong&gt; with file system &lt;strong&gt;%2&lt;/strong&gt;.</source>
        <translation>Форматиране на &lt;strong&gt;%3MiB &lt;/strong&gt; дял &lt;strong&gt;%1 &lt;/strong&gt; с файлова система&lt;strong&gt;%2 &lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FormatPartitionJob.cpp" line="58"/>
        <source>Formatting partition %1 with file system %2.</source>
        <translation>Форматиране на дял %1 с файлова система %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/FormatPartitionJob.cpp" line="72"/>
        <source>The installer failed to format partition %1 on disk &apos;%2&apos;.</source>
        <translation>Инсталаторът не успя да форматира дял %1 на диск &apos;%2&apos;.</translation>
    </message>
</context>
<context>
    <name>GeneralRequirements</name>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="149"/>
        <source>has at least %1 GiB available drive space</source>
        <translation>има поне %1 GiB свободно място на диска</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="151"/>
        <source>There is not enough drive space. At least %1 GiB is required.</source>
        <translation>Няма достатъчно място на диска. Необходими са най-малко %1 GiB.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="160"/>
        <source>has at least %1 GiB working memory</source>
        <translation>има поне %1 GiB работна памет</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="162"/>
        <source>The system does not have enough working memory. At least %1 GiB is required.</source>
        <translation>Системата няма достатъчно работна памет. Необходими са поне %1 GIB.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="171"/>
        <source>is plugged in to a power source</source>
        <translation>е включен към източник на захранване</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="172"/>
        <source>The system is not plugged in to a power source.</source>
        <translation>Системата не е включена към източник на захранване.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="179"/>
        <source>is connected to the Internet</source>
        <translation>е свързан към интернет</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="180"/>
        <source>The system is not connected to the Internet.</source>
        <translation>Системата не е свързана с интернет.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="187"/>
        <source>is running the installer as an administrator (root)</source>
        <translation>изпълнява инсталатора като администратор (root)</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="190"/>
        <source>The setup program is not running with administrator rights.</source>
        <translation>Програмата за настройване не се изпълнява с права на администратор.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="191"/>
        <source>The installer is not running with administrator rights.</source>
        <translation>Инсталаторът не е стартиран с права на администратор.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="199"/>
        <source>has a screen large enough to show the whole installer</source>
        <translation>Има екран, достатъчно голям, за да покаже целия прозорец на инсталатора.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="202"/>
        <source>The screen is too small to display the setup program.</source>
        <translation>Екранът е твърде малък, за да се покаже програмата за инсталиране.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/GeneralRequirements.cpp" line="203"/>
        <source>The screen is too small to display the installer.</source>
        <translation>Екранът е твърде малък за инсталатора.</translation>
    </message>
</context>
<context>
    <name>HostInfoJob</name>
    <message>
        <location filename="../src/modules/hostinfo/HostInfoJob.cpp" line="42"/>
        <source>Collecting information about your machine.</source>
        <translation>Събиране на информация за вашата машина.</translation>
    </message>
</context>
<context>
    <name>IDJob</name>
    <message>
        <location filename="../src/modules/oemid/IDJob.cpp" line="30"/>
        <location filename="../src/modules/oemid/IDJob.cpp" line="39"/>
        <location filename="../src/modules/oemid/IDJob.cpp" line="52"/>
        <location filename="../src/modules/oemid/IDJob.cpp" line="59"/>
        <source>OEM Batch Identifier</source>
        <translation>OEM -партиден идентификатор</translation>
    </message>
    <message>
        <location filename="../src/modules/oemid/IDJob.cpp" line="40"/>
        <source>Could not create directories &lt;code&gt;%1&lt;/code&gt;.</source>
        <translation>Неуспех при създаването на директории &lt;code&gt;%1 &lt;/code&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/oemid/IDJob.cpp" line="53"/>
        <source>Could not open file &lt;code&gt;%1&lt;/code&gt;.</source>
        <translation>Грешка при отваряне на файла &lt;code&gt;%1&lt;/code&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/oemid/IDJob.cpp" line="60"/>
        <source>Could not write to file &lt;code&gt;%1&lt;/code&gt;.</source>
        <translation>Не може да се извърши запис във файл &lt;code&gt;%1&lt;/code&gt;.</translation>
    </message>
</context>
<context>
    <name>InitcpioJob</name>
    <message>
        <location filename="../src/modules/initcpio/InitcpioJob.cpp" line="31"/>
        <source>Creating initramfs with mkinitcpio.</source>
        <translation>Създаване на initramfs с mkinitcpio.</translation>
    </message>
</context>
<context>
    <name>InitramfsJob</name>
    <message>
        <location filename="../src/modules/initramfs/InitramfsJob.cpp" line="28"/>
        <source>Creating initramfs.</source>
        <translation>Създаване на initramfs.</translation>
    </message>
</context>
<context>
    <name>InteractiveTerminalPage</name>
    <message>
        <location filename="../src/modules/interactiveterminal/InteractiveTerminalPage.cpp" line="44"/>
        <source>Konsole not installed</source>
        <translation>Konsole не е инсталиран</translation>
    </message>
    <message>
        <location filename="../src/modules/interactiveterminal/InteractiveTerminalPage.cpp" line="44"/>
        <source>Please install KDE Konsole and try again!</source>
        <translation>Моля, инсталирайте KDE Konsole и опитайте отново!</translation>
    </message>
    <message>
        <location filename="../src/modules/interactiveterminal/InteractiveTerminalPage.cpp" line="102"/>
        <source>Executing script: &amp;nbsp;&lt;code&gt;%1&lt;/code&gt;</source>
        <translation>Изпълняване на скрипт: &amp;nbsp;&lt;code&gt;%1&lt;/code&gt;</translation>
    </message>
</context>
<context>
    <name>InteractiveTerminalViewStep</name>
    <message>
        <location filename="../src/modules/interactiveterminal/InteractiveTerminalViewStep.cpp" line="41"/>
        <source>Script</source>
        <translation>Скрипт</translation>
    </message>
</context>
<context>
    <name>KeyboardQmlViewStep</name>
    <message>
        <location filename="../src/modules/keyboardq/KeyboardQmlViewStep.cpp" line="32"/>
        <source>Keyboard</source>
        <translation>Клавиатура</translation>
    </message>
</context>
<context>
    <name>KeyboardViewStep</name>
    <message>
        <location filename="../src/modules/keyboard/KeyboardViewStep.cpp" line="42"/>
        <source>Keyboard</source>
        <translation>Клавиатура</translation>
    </message>
</context>
<context>
    <name>LCLocaleDialog</name>
    <message>
        <location filename="../src/modules/locale/LCLocaleDialog.cpp" line="23"/>
        <source>System locale setting</source>
        <translation>Регионални настройки на системата</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/LCLocaleDialog.cpp" line="30"/>
        <source>The system locale setting affects the language and character set for some command line user interface elements.&lt;br/&gt;The current setting is &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Регионалните настройки на системата засягат езика и символите, зададени за някои елементи на командния ред.&lt;br/&gt;Текущата настройка е &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/LCLocaleDialog.cpp" line="54"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отказ</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/LCLocaleDialog.cpp" line="55"/>
        <source>&amp;OK</source>
        <translation>&amp;Добре</translation>
    </message>
</context>
<context>
    <name>LicensePage</name>
    <message>
        <location filename="../src/modules/license/LicensePage.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.ui" line="26"/>
        <source>&lt;h1&gt;License Agreement&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Лицензно споразумение&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.cpp" line="136"/>
        <source>I accept the terms and conditions above.</source>
        <translation>Приемам лицензионните условия.</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.cpp" line="138"/>
        <source>Please review the End User License Agreements (EULAs).</source>
        <translation>Моля, прегледайте лицензионните споразумения за краен потребител (EULAS).</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.cpp" line="143"/>
        <source>This setup procedure will install proprietary software that is subject to licensing terms.</source>
        <translation>Тази процедура за настройка ще инсталира патентован софтуер, който подлежи на лицензионни условия.</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.cpp" line="146"/>
        <source>If you do not agree with the terms, the setup procedure cannot continue.</source>
        <translation>Ако не сте съгласни с условията, процедурата за инсталиране не може да продължи.</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.cpp" line="151"/>
        <source>This setup procedure can install proprietary software that is subject to licensing terms in order to provide additional features and enhance the user experience.</source>
        <translation>С цел да се осигурят допълнителни функции и да се подобри работата на потребителя, процедурата може да инсталира софтуер, който е обект на лицензионни условия.</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicensePage.cpp" line="156"/>
        <source>If you do not agree with the terms, proprietary software will not be installed, and open source alternatives will be used instead.</source>
        <translation>Ако не сте съгласни с условията, патентованият софтуер няма да бъде инсталиран и вместо него ще бъдат използвани алтернативи с отворен код.</translation>
    </message>
</context>
<context>
    <name>LicenseViewStep</name>
    <message>
        <location filename="../src/modules/license/LicenseViewStep.cpp" line="43"/>
        <source>License</source>
        <translation>Лиценз</translation>
    </message>
</context>
<context>
    <name>LicenseWidget</name>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="88"/>
        <source>URL: %1</source>
        <translation>Адрес: %1</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="109"/>
        <source>&lt;strong&gt;%1 driver&lt;/strong&gt;&lt;br/&gt;by %2</source>
        <extracomment>%1 is an untranslatable product name, example: Creative Audigy driver</extracomment>
        <translation>&lt;strong&gt;%1 драйвер&lt;/strong&gt;&lt;br/&gt;от %2</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="116"/>
        <source>&lt;strong&gt;%1 graphics driver&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;by %2&lt;/font&gt;</source>
        <extracomment>%1 is usually a vendor name, example: Nvidia graphics driver</extracomment>
        <translation>&lt;strong&gt;%1 графичен драйвер&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;от %2&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="122"/>
        <source>&lt;strong&gt;%1 browser plugin&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;by %2&lt;/font&gt;</source>
        <translation>&lt;strong&gt;%1 плъгин за браузър&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;от %2&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="128"/>
        <source>&lt;strong&gt;%1 codec&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;by %2&lt;/font&gt;</source>
        <translation>&lt;strong&gt;%1 кодек&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;от %2&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="134"/>
        <source>&lt;strong&gt;%1 package&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;by %2&lt;/font&gt;</source>
        <translation>&lt;strong&gt;%1 пакет&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;от %2&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="140"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;by %2&lt;/font&gt;</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt;&lt;br/&gt;&lt;font color=&quot;Grey&quot;&gt;от %2&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="163"/>
        <source>File: %1</source>
        <translation>Файл: %1</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="186"/>
        <source>Hide license text</source>
        <translation>Скриване на текста на лиценза</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="186"/>
        <source>Show the license text</source>
        <translation>Показване на текста на лиценза</translation>
    </message>
    <message>
        <location filename="../src/modules/license/LicenseWidget.cpp" line="190"/>
        <source>Open license agreement in browser.</source>
        <translation>Отваряне на лицензионното споразумение в браузъра.</translation>
    </message>
</context>
<context>
    <name>LocalePage</name>
    <message>
        <location filename="../src/modules/locale/LocalePage.cpp" line="130"/>
        <source>Region:</source>
        <translation>Регион:</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/LocalePage.cpp" line="131"/>
        <source>Zone:</source>
        <translation>Зона:</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/LocalePage.cpp" line="132"/>
        <location filename="../src/modules/locale/LocalePage.cpp" line="133"/>
        <source>&amp;Change...</source>
        <translation>&amp;Промени...</translation>
    </message>
</context>
<context>
    <name>LocaleQmlViewStep</name>
    <message>
        <location filename="../src/modules/localeq/LocaleQmlViewStep.cpp" line="32"/>
        <source>Location</source>
        <translation>Местоположение</translation>
    </message>
</context>
<context>
    <name>LocaleViewStep</name>
    <message>
        <location filename="../src/modules/locale/LocaleViewStep.cpp" line="76"/>
        <source>Location</source>
        <translation>Местоположение</translation>
    </message>
</context>
<context>
    <name>LuksBootKeyFileJob</name>
    <message>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="28"/>
        <source>Configuring LUKS key file.</source>
        <translation>Конфигуриране на ключов файл LUKS.</translation>
    </message>
    <message>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="168"/>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="176"/>
        <source>No partitions are defined.</source>
        <translation>Няма зададени дялове.</translation>
    </message>
    <message>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="211"/>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="218"/>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="226"/>
        <source>Encrypted rootfs setup error</source>
        <translation>Грешка при настройване на криптирана rootfs</translation>
    </message>
    <message>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="212"/>
        <source>Root partition %1 is LUKS but no passphrase has been set.</source>
        <translation>Root дял %1 е LUKS, но не е зададена парола.</translation>
    </message>
    <message>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="219"/>
        <source>Could not create LUKS key file for root partition %1.</source>
        <translation>Не можа да се създаде ключов файл LUKS за root дял %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/luksbootkeyfile/LuksBootKeyFileJob.cpp" line="227"/>
        <source>Could not configure LUKS key file on partition %1.</source>
        <translation>Неуспешно конфигуриране на ключов файл на LUKS на дял %1.</translation>
    </message>
</context>
<context>
    <name>MachineIdJob</name>
    <message>
        <location filename="../src/modules/machineid/MachineIdJob.cpp" line="37"/>
        <source>Generate machine-id.</source>
        <translation>Генериране на machine-id.</translation>
    </message>
    <message>
        <location filename="../src/modules/machineid/MachineIdJob.cpp" line="53"/>
        <source>Configuration Error</source>
        <translation>Грешка в конфигурацията</translation>
    </message>
    <message>
        <location filename="../src/modules/machineid/MachineIdJob.cpp" line="54"/>
        <source>No root mount point is set for MachineId.</source>
        <translation>Не е зададена точка за монтиране на root за MachineID.</translation>
    </message>
</context>
<context>
    <name>Map</name>
    <message>
        <location filename="../src/modules/localeq/Map.qml" line="243"/>
        <source>Timezone: %1</source>
        <translation>Часова зона: %1</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/Map.qml" line="264"/>
        <source>Please select your preferred location on the map so the installer can suggest the locale
            and timezone settings for you. You can fine-tune the suggested settings below. Search the map by dragging
            to move and using the +/- buttons to zoom in/out or use mouse scrolling for zooming.</source>
        <translation>Моля, изберете предпочитаното от вас местоположение на картата, за да може инсталаторът да предложи съответните регионални настройки
            и настройките на часовия пояс. Можете да направите точна корекция на предложените настройки по-долу. Премествайте картата с влачене
             и с помощта на бутоните +/-  или колелцето на мишката променяйте мащаба, за да намерите местоположението.</translation>
    </message>
</context>
<context>
    <name>NetInstallViewStep</name>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="47"/>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="54"/>
        <source>Package selection</source>
        <translation>Избор на пакети</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="55"/>
        <source>Office software</source>
        <translation>Офис софтуер</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="56"/>
        <source>Office package</source>
        <translation>Офис пакет</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="57"/>
        <source>Browser software</source>
        <translation>Софтуер за браузър</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="58"/>
        <source>Browser package</source>
        <translation>Пакет на браузър</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="59"/>
        <source>Web browser</source>
        <translation>Уеб браузър</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="60"/>
        <source>Kernel</source>
        <translation>Ядро</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="61"/>
        <source>Services</source>
        <translation>Услуги</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="62"/>
        <source>Login</source>
        <translation>Вход в системата</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="63"/>
        <source>Desktop</source>
        <translation>Работен плот</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="64"/>
        <source>Applications</source>
        <translation>Приложения</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="65"/>
        <source>Communication</source>
        <translation>Комуникация</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="66"/>
        <source>Development</source>
        <translation>За разработчици</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="67"/>
        <source>Office</source>
        <translation>Офис</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="68"/>
        <source>Multimedia</source>
        <translation>Мултимедия</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="69"/>
        <source>Internet</source>
        <translation>Интернет</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="70"/>
        <source>Theming</source>
        <translation>Стилове и теми</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="71"/>
        <source>Gaming</source>
        <translation>Игри</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/NetInstallViewStep.cpp" line="72"/>
        <source>Utilities</source>
        <translation>Помощни програми</translation>
    </message>
</context>
<context>
    <name>NotesQmlViewStep</name>
    <message>
        <location filename="../src/modules/notesqml/NotesQmlViewStep.cpp" line="23"/>
        <source>Notes</source>
        <translation>Бележки</translation>
    </message>
</context>
<context>
    <name>OEMPage</name>
    <message>
        <location filename="../src/modules/oemid/OEMPage.ui" line="32"/>
        <source>Ba&amp;tch:</source>
        <translation>Паке&amp;тни команди:</translation>
    </message>
    <message>
        <location filename="../src/modules/oemid/OEMPage.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter a batch-identifier here. This will be stored in the target system.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Въведете идентификатор за пакетните команди тук. Той ще се съхранява в целевата система&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/oemid/OEMPage.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;OEM Configuration&lt;/h1&gt;&lt;p&gt;Calamares will use OEM settings while configuring the target system.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;OEM Конфигурация&lt;/h1&gt;&lt;p&gt;Calamares ще използва OEM настройки при конфигуриране на целевата система&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>OEMViewStep</name>
    <message>
        <location filename="../src/modules/oemid/OEMViewStep.cpp" line="122"/>
        <source>OEM Configuration</source>
        <translation>OEM конфигурация</translation>
    </message>
    <message>
        <location filename="../src/modules/oemid/OEMViewStep.cpp" line="128"/>
        <source>Set the OEM Batch Identifier to &lt;code&gt;%1&lt;/code&gt;.</source>
        <translation>Задаване на идентификатора на OEM пакетни команди на &lt;code&gt;%1 &lt;/code&gt;.</translation>
    </message>
</context>
<context>
    <name>Offline</name>
    <message>
        <location filename="../src/modules/localeq/Offline.qml" line="37"/>
        <source>Select your preferred Region, or use the default one based on your current location.</source>
        <translation>Изберете предпочитания от вас регион или използвайте настройките по подразбиране.</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/Offline.qml" line="94"/>
        <location filename="../src/modules/localeq/Offline.qml" line="169"/>
        <location filename="../src/modules/localeq/Offline.qml" line="213"/>
        <source>Timezone: %1</source>
        <translation>Часова зона: %1</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/Offline.qml" line="111"/>
        <source>Select your preferred Zone within your Region.</source>
        <translation>Изберете предпочитаната от вас зона във вашия регион.</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/Offline.qml" line="182"/>
        <source>Zones</source>
        <translation>Зони</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/Offline.qml" line="229"/>
        <source>You can fine-tune Language and Locale settings below.</source>
        <translation>Можете да прецизирате настройките за езика и регионалните формати по-долу.</translation>
    </message>
</context>
<context>
    <name>PWQ</name>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="51"/>
        <source>Password is too short</source>
        <translation>Паролата е твърде кратка</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="67"/>
        <source>Password is too long</source>
        <translation>Паролата е твърде дълга</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="188"/>
        <source>Password is too weak</source>
        <translation>Паролата е твърде слаба</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="196"/>
        <source>Memory allocation error when setting &apos;%1&apos;</source>
        <translation>Грешка при разпределяне на паметта по време на настройването на &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="199"/>
        <source>Memory allocation error</source>
        <translation>Грешка при разпределяне на паметта</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="201"/>
        <source>The password is the same as the old one</source>
        <translation>Паролата съвпада с предишната</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="203"/>
        <source>The password is a palindrome</source>
        <translation>Паролата е палиндром</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="205"/>
        <source>The password differs with case changes only</source>
        <translation>Паролата се различава само със смяна на главни и малки букви</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="207"/>
        <source>The password is too similar to the old one</source>
        <translation>Паролата е твърде сходна с предишната</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="209"/>
        <source>The password contains the user name in some form</source>
        <translation>Паролата съдържа потребителското име под някаква форма</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="211"/>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>Паролата съдържа думи от истинското име на потребителя под някаква форма</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="214"/>
        <source>The password contains forbidden words in some form</source>
        <translation>Паролата съдържа забранени думи под някаква форма</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="221"/>
        <source>The password contains too few digits</source>
        <translation>Паролата съдържа твърде малко цифри</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="228"/>
        <source>The password contains too few uppercase letters</source>
        <translation>Паролата съдържа твърде малко главни букви</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="232"/>
        <source>The password contains fewer than %n lowercase letters</source>
        <translation>
            <numerusform>Паролата съдържа по -малко от %n малки букви</numerusform>
            <numerusform>Паролата съдържа по -малко от %n малки букви</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="235"/>
        <source>The password contains too few lowercase letters</source>
        <translation>Паролата съдържа твърде малко малки букви</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="242"/>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>Паролата съдържа твърде малко знаци, които не са букви или цифри</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="249"/>
        <source>The password is too short</source>
        <translation>Паролата е твърде кратка</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="258"/>
        <source>The password does not contain enough character classes</source>
        <translation>Паролата не съдържа достатъчно видове знаци</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="265"/>
        <source>The password contains too many same characters consecutively</source>
        <translation>Паролата съдържа твърде много еднакви знаци последователно</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="275"/>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>Паролата съдържа твърде много еднакви видове знаци последователно</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="218"/>
        <source>The password contains fewer than %n digits</source>
        <translation>
            <numerusform>Паролата съдържа по -малко от %n цифри</numerusform>
            <numerusform>Паролата съдържа по -малко от %n цифри</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="225"/>
        <source>The password contains fewer than %n uppercase letters</source>
        <translation>
            <numerusform>Паролата съдържа по -малко от %n главни букви</numerusform>
            <numerusform>Паролата съдържа по -малко от %n главни букви</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="239"/>
        <source>The password contains fewer than %n non-alphanumeric characters</source>
        <translation>
            <numerusform>Паролата съдържа по-малко от %n небуквени и нецифрови знаци</numerusform>
            <numerusform>Паролата съдържа по-малко от %n небуквени и нецифрови знаци</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="246"/>
        <source>The password is shorter than %n characters</source>
        <translation>
            <numerusform>Паролата е по -къса от %n знака</numerusform>
            <numerusform>Паролата е по -къса от %n знака</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="251"/>
        <source>The password is a rotated version of the previous one</source>
        <translation>Паролата е обърната версия на предишната</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="255"/>
        <source>The password contains fewer than %n character classes</source>
        <translation>
            <numerusform>Паролата съдържа по -малко от %n класове символи</numerusform>
            <numerusform>Паролата съдържа по -малко от %n класове символи</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="262"/>
        <source>The password contains more than %n same characters consecutively</source>
        <translation>
            <numerusform>Паролата съдържа повече от %n еднакви знака последователно</numerusform>
            <numerusform>Паролата съдържа повече от %n еднакви знака последователно</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="269"/>
        <source>The password contains more than %n characters of the same class consecutively</source>
        <translation>
            <numerusform>Паролата съдържа повече от %n знака от един и същи клас последователно</numerusform>
            <numerusform>Паролата съдържа повече от %n знака от един и същи клас последователно</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="280"/>
        <source>The password contains monotonic sequence longer than %n characters</source>
        <translation>
            <numerusform>Паролата съдържа монотонната последователност по -дълга от %n знаци</numerusform>
            <numerusform>Паролата съдържа монотонната последователност по -дълга от %n знаци</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="286"/>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>Паролата съдържа твърде дълга монотонна последователност от знаци</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="289"/>
        <source>No password supplied</source>
        <translation>Липсва парола</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="291"/>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>Получаването на произволни числа от RNG устройството е неуспешно</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="293"/>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>Генерирането на парола е неуспешно - необходимата ентропия е твърде ниска за настройки</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="298"/>
        <source>The password fails the dictionary check - %1</source>
        <translation>Паролата не издържа проверката на речника - %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="301"/>
        <source>The password fails the dictionary check</source>
        <translation>Паролата не издържа проверката на речника</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="305"/>
        <source>Unknown setting - %1</source>
        <translation>Неизвестна настройка - %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="307"/>
        <source>Unknown setting</source>
        <translation>Неизвестна настройка</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="311"/>
        <source>Bad integer value of setting - %1</source>
        <translation>Невалидна числена стойност на настройката - %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="313"/>
        <source>Bad integer value</source>
        <translation>Невалидна числена стойност на настройката</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="317"/>
        <source>Setting %1 is not of integer type</source>
        <translation>Настройката %1 не е от числов вид</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="319"/>
        <source>Setting is not of integer type</source>
        <translation>Настройката не е от числов вид</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="323"/>
        <source>Setting %1 is not of string type</source>
        <translation>Настройката %1 не е от текстов вид</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="325"/>
        <source>Setting is not of string type</source>
        <translation>Настройката не е от текстов вид</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="327"/>
        <source>Opening the configuration file failed</source>
        <translation>Отварянето на файла с конфигурацията е неуспешно</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="329"/>
        <source>The configuration file is malformed</source>
        <translation>Файлът с конфигурацията е деформиран</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="331"/>
        <source>Fatal failure</source>
        <translation>Фатална повреда</translation>
    </message>
    <message>
        <location filename="../src/modules/users/CheckPWQuality.cpp" line="333"/>
        <source>Unknown error</source>
        <translation>Неизвестна грешка</translation>
    </message>
    <message>
        <location filename="../src/modules/users/Config.cpp" line="775"/>
        <source>Password is empty</source>
        <translation>Паролата е празна</translation>
    </message>
</context>
<context>
    <name>PackageChooserPage</name>
    <message>
        <location filename="../src/modules/packagechooser/page_package.ui" line="24"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/page_package.ui" line="44"/>
        <source>Product Name</source>
        <translation>Продуктово име</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/page_package.ui" line="57"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/page_package.ui" line="73"/>
        <source>Long Product Description</source>
        <translation>Подробно описание на продукта</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/PackageChooserPage.cpp" line="25"/>
        <source>Package Selection</source>
        <translation>Избор на пакети</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/PackageChooserPage.cpp" line="26"/>
        <source>Please pick a product from the list. The selected product will be installed.</source>
        <translation>Моля, изберете продукт от списъка. Избраният продукт ще бъде инсталиран.</translation>
    </message>
</context>
<context>
    <name>PackageChooserViewStep</name>
    <message>
        <location filename="../src/modules/packagechooser/PackageChooserViewStep.cpp" line="61"/>
        <source>Packages</source>
        <translation>Пакети</translation>
    </message>
</context>
<context>
    <name>PackageModel</name>
    <message>
        <location filename="../src/modules/netinstall/PackageModel.cpp" line="168"/>
        <source>Name</source>
        <translation>Име</translation>
    </message>
    <message>
        <location filename="../src/modules/netinstall/PackageModel.cpp" line="168"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
</context>
<context>
    <name>Page_Keyboard</name>
    <message>
        <location filename="../src/modules/keyboard/KeyboardPage.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/KeyboardPage.ui" line="74"/>
        <source>Keyboard Model:</source>
        <translation>Модел на клавиатура:</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/KeyboardPage.ui" line="135"/>
        <source>Type here to test your keyboard</source>
        <translation>Пишете тук за да тествате вашата клавиатура</translation>
    </message>
</context>
<context>
    <name>Page_UserSetup</name>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="40"/>
        <source>What is your name?</source>
        <translation>Какво е вашето име?</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="55"/>
        <source>Your Full Name</source>
        <translation>Вашето пълно име</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="124"/>
        <source>What name do you want to use to log in?</source>
        <translation>Какво име искате да използвате за влизане?</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="148"/>
        <source>login</source>
        <translation>вход</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="223"/>
        <source>What is the name of this computer?</source>
        <translation>Какво е името на този компютър?</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="247"/>
        <source>&lt;small&gt;This name will be used if you make the computer visible to others on a network.&lt;/small&gt;</source>
        <translation>&lt;small&gt;Това име ще бъде използвано ако направите компютъра видим за други при мрежа.&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="250"/>
        <source>Computer Name</source>
        <translation>Име на компютър</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="325"/>
        <source>Choose a password to keep your account safe.</source>
        <translation>Изберете парола за да държите вашият акаунт в безопасност.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="349"/>
        <location filename="../src/modules/users/page_usersetup.ui" line="374"/>
        <source>&lt;small&gt;Enter the same password twice, so that it can be checked for typing errors. A good password will contain a mixture of letters, numbers and punctuation, should be at least eight characters long, and should be changed at regular intervals.&lt;/small&gt;</source>
        <translation>&lt;small&gt;Въведете същата парола два пъти, за да може да бъде проверена за правописни грешки. Добрата парола трябва да съдържа смесица от букви, цифри и пунктуационни знаци, трябва да бъде поне с осем знака и да бъде променяна често.&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="355"/>
        <location filename="../src/modules/users/page_usersetup.ui" line="525"/>
        <source>Password</source>
        <translation>Парола</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="380"/>
        <location filename="../src/modules/users/page_usersetup.ui" line="550"/>
        <source>Repeat Password</source>
        <translation>Повтаряне на паролата</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="455"/>
        <source>When this box is checked, password-strength checking is done and you will not be able to use a weak password.</source>
        <translation>Ако това поле е маркирано, се извършва проверка на силата на паролата и няма да можете да използвате слаба парола.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="458"/>
        <source>Require strong passwords.</source>
        <translation>Изискване на силни пароли.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="465"/>
        <source>Log in automatically without asking for the password.</source>
        <translation>Автоматично влизане, без питане за паролата.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="472"/>
        <source>Use the same password for the administrator account.</source>
        <translation>Използване на същата парола за администраторския акаунт.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="495"/>
        <source>Choose a password for the administrator account.</source>
        <translation>Изберете парола за администраторския акаунт.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/page_usersetup.ui" line="519"/>
        <location filename="../src/modules/users/page_usersetup.ui" line="544"/>
        <source>&lt;small&gt;Enter the same password twice, so that it can be checked for typing errors.&lt;/small&gt;</source>
        <translation>&lt;small&gt;Въведете същата парола два пъти, за да може да бъде проверена за правописни грешки.&lt;/small&gt;</translation>
    </message>
</context>
<context>
    <name>PartitionLabelsView</name>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="190"/>
        <source>Root</source>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="194"/>
        <source>Home</source>
        <translation>Домашна папка</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="198"/>
        <source>Boot</source>
        <translation>Boot</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="203"/>
        <source>EFI system</source>
        <translation>EFI система</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="207"/>
        <source>Swap</source>
        <translation>Swap</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="211"/>
        <source>New partition for %1</source>
        <translation>Нов дял за %1</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="215"/>
        <source>New partition</source>
        <translation>Нов дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="239"/>
        <source>%1  %2</source>
        <extracomment>size[number] filesystem[name]</extracomment>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>PartitionModel</name>
    <message>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="159"/>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="199"/>
        <source>Free Space</source>
        <translation>Свободно пространство</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="163"/>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="203"/>
        <source>New partition</source>
        <translation>Нов дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="296"/>
        <source>Name</source>
        <translation>Име</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="298"/>
        <source>File System</source>
        <translation>Файлова система</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="300"/>
        <source>Mount Point</source>
        <translation>Точка на монтиране</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/core/PartitionModel.cpp" line="302"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
</context>
<context>
    <name>PartitionPage</name>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="26"/>
        <source>Storage de&amp;vice:</source>
        <translation>Ус&amp;тройство за съхранение:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="55"/>
        <source>&amp;Revert All Changes</source>
        <translation>&amp;Нулиране на всички промени</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="91"/>
        <source>New Partition &amp;Table</source>
        <translation>Нова &amp;таблица на дяловете</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="111"/>
        <source>Cre&amp;ate</source>
        <translation>Съз&amp;даване</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="118"/>
        <source>&amp;Edit</source>
        <translation>&amp;Редактиране</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="125"/>
        <source>&amp;Delete</source>
        <translation>&amp;Изтриване на</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="136"/>
        <source>New Volume Group</source>
        <translation>Нова група дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="143"/>
        <source>Resize Volume Group</source>
        <translation>Преоразмеряване на група дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="150"/>
        <source>Deactivate Volume Group</source>
        <translation>Деактивиране на група дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="157"/>
        <source>Remove Volume Group</source>
        <translation>Премахване на група дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.ui" line="184"/>
        <source>I&amp;nstall boot loader on:</source>
        <translation>И&amp;нсталиране на програма за начално зареждане на:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.cpp" line="219"/>
        <source>Are you sure you want to create a new partition table on %1?</source>
        <translation>Сигурни ли сте че искате да създадете нова таблица на дяловете върху %1?</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.cpp" line="254"/>
        <source>Can not create new partition</source>
        <translation>Не може да се създаде нов дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionPage.cpp" line="255"/>
        <source>The partition table on %1 already has %2 primary partitions, and no more can be added. Please remove one primary partition and add an extended partition, instead.</source>
        <translation>Таблицата на дяловете на %1 вече има %2 главни дялове, повече не може да се добавят. Моля, премахнете един главен дял и добавете разширен дял, на негово място.</translation>
    </message>
</context>
<context>
    <name>PartitionViewStep</name>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="70"/>
        <source>Gathering system information...</source>
        <translation>Събиране на системна информация...</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="125"/>
        <source>Partitions</source>
        <translation>Дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="163"/>
        <source>Install %1 &lt;strong&gt;alongside&lt;/strong&gt; another operating system.</source>
        <translation>Инсталиране на %1 &lt;strong&gt;заедно&lt;/strong&gt; с друга операционна система.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="167"/>
        <source>&lt;strong&gt;Erase&lt;/strong&gt; disk and install %1.</source>
        <translation>&lt;strong&gt;Изтриване на&lt;/strong&gt; диска и инсталиране на %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="170"/>
        <source>&lt;strong&gt;Replace&lt;/strong&gt; a partition with %1.</source>
        <translation>&lt;strong&gt;Заменяне на&lt;/strong&gt; дял с %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="174"/>
        <source>&lt;strong&gt;Manual&lt;/strong&gt; partitioning.</source>
        <translation>&lt;strong&gt;Ръчно&lt;/strong&gt; поделяне.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="187"/>
        <source>Install %1 &lt;strong&gt;alongside&lt;/strong&gt; another operating system on disk &lt;strong&gt;%2&lt;/strong&gt; (%3).</source>
        <translation>Инсталиране на %1 &lt;strong&gt;заедно&lt;/strong&gt; с друга операционна система на диск &lt;strong&gt;%2&lt;/strong&gt; (%3).</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="194"/>
        <source>&lt;strong&gt;Erase&lt;/strong&gt; disk &lt;strong&gt;%2&lt;/strong&gt; (%3) and install %1.</source>
        <translation>&lt;strong&gt;Изтриване на&lt;/strong&gt; диск &lt;strong&gt;%2&lt;/strong&gt; (%3) и инсталиране на %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="200"/>
        <source>&lt;strong&gt;Replace&lt;/strong&gt; a partition on disk &lt;strong&gt;%2&lt;/strong&gt; (%3) with %1.</source>
        <translation>&lt;strong&gt;Заменяне на&lt;/strong&gt; дял на диск &lt;strong&gt;%2&lt;/strong&gt; (%3) с %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="207"/>
        <source>&lt;strong&gt;Manual&lt;/strong&gt; partitioning on disk &lt;strong&gt;%1&lt;/strong&gt; (%2).</source>
        <translation>&lt;strong&gt;Ръчно&lt;/strong&gt; поделяне на диск &lt;strong&gt;%1&lt;/strong&gt; (%2).</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="216"/>
        <source>Disk &lt;strong&gt;%1&lt;/strong&gt; (%2)</source>
        <translation>Диск &lt;strong&gt;%1&lt;/strong&gt; (%2)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="242"/>
        <source>Current:</source>
        <translation>Сегашен:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="260"/>
        <source>After:</source>
        <translation>След:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="427"/>
        <source>No EFI system partition configured</source>
        <translation>Няма конфигуриран EFI системен дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="428"/>
        <source>An EFI system partition is necessary to start %1.&lt;br/&gt;&lt;br/&gt;To configure an EFI system partition, go back and select or create a FAT32 filesystem with the &lt;strong&gt;%3&lt;/strong&gt; flag enabled and mount point &lt;strong&gt;%2&lt;/strong&gt;.&lt;br/&gt;&lt;br/&gt;You can continue without setting up an EFI system partition but your system may fail to start.</source>
        <translation>За стартирането на %1 е необходим системен дял EFI.&lt;br/&gt;&lt;br/&gt;За да конфигурирате системен дял EFI, върнете се назад и изберете или създайте файлова система FAT32 с активиран флаг &lt;strong&gt;%3&lt;/strong&gt; и точка на монтиране &lt;strong&gt;%2&lt;/strong&gt;.&lt;br/&gt;&lt;br/&gt;Можете да продължите, без да конфигурирате системен дял EFI, но системата ви може да не успее да се стартира.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="442"/>
        <source>An EFI system partition is necessary to start %1.&lt;br/&gt;&lt;br/&gt;A partition was configured with mount point &lt;strong&gt;%2&lt;/strong&gt; but its &lt;strong&gt;%3&lt;/strong&gt; flag is not set.&lt;br/&gt;To set the flag, go back and edit the partition.&lt;br/&gt;&lt;br/&gt;You can continue without setting the flag but your system may fail to start.</source>
        <translation>За стартирането на %1 е необходим EFI системен дял.&lt;br/&gt;&lt;br/&gt;Дял е конфигуриран с точка на монтиране &lt;strong&gt;%2&lt;/strong&gt;, но флагът му &lt;strong&gt;%3&lt;/strong&gt; не е зададен.&lt;br/&gt;За да зададете флага, върнете се и редактирайте дяла.&lt;br/&gt;&lt;br/&gt;Можете да продължите без задаване на флага, но системата ви може да не успее да се стартира.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="441"/>
        <source>EFI system partition flag not set</source>
        <translation>Не е зададен флаг на EFI системен дял</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="468"/>
        <source>Option to use GPT on BIOS</source>
        <translation>Опция за използване на GPT на BIOS</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="469"/>
        <source>A GPT partition table is the best option for all systems. This installer supports such a setup for BIOS systems too.&lt;br/&gt;&lt;br/&gt;To configure a GPT partition table on BIOS, (if not done so already) go back and set the partition table to GPT, next create a 8 MB unformatted partition with the &lt;strong&gt;bios_grub&lt;/strong&gt; flag enabled.&lt;br/&gt;&lt;br/&gt;An unformatted 8 MB partition is necessary to start %1 on a BIOS system with GPT.</source>
        <translation>Таблица с дялове на GPT е най -добрият вариант за всички системи. Този инсталатор поддържа такава настройка и за BIOS системи. &lt;br/&gt;&lt;br/&gt; За конфигуриране на GPT таблица с дяловете в BIOS (ако вече не сте го направили), върнете се назад и задайте таблица на дяловете на GPT, след което създайте 8 MB неформатиран дял с активиран  &lt;strong&gt;bios_grub&lt;/strong&gt;флаг. &lt;br/&gt;&lt;br/&gt; Необходим е 8 MB дял за стартиране на %1 на BIOS система с GPT.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="497"/>
        <source>Boot partition not encrypted</source>
        <translation>Липсва криптиране на дял за начално зареждане</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="498"/>
        <source>A separate boot partition was set up together with an encrypted root partition, but the boot partition is not encrypted.&lt;br/&gt;&lt;br/&gt;There are security concerns with this kind of setup, because important system files are kept on an unencrypted partition.&lt;br/&gt;You may continue if you wish, but filesystem unlocking will happen later during system startup.&lt;br/&gt;To encrypt the boot partition, go back and recreate it, selecting &lt;strong&gt;Encrypt&lt;/strong&gt; in the partition creation window.</source>
        <translation>Отделен дял за начално зареждане беше създаден заедно с криптиран root дял, но не беше криптиран. &lt;br/&gt;&lt;br/&gt;При този вид настройка има проблеми със сигурността, тъй като важни системни файлове се съхраняват на некриптиран дял.&lt;br/&gt; Можете да продължите, ако желаете, но отключването на файловата система ще се случи по -късно по време на стартиране на системата. &lt;br/&gt; За да криптирате дялът за начално зареждане, върнете се назад и го създайте отново, избирайки &lt;strong&gt;Криптиране&lt;/strong&gt; в прозореца за създаване на дяла.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="627"/>
        <source>has at least one disk device available.</source>
        <translation>има поне едно дисково устройство.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionViewStep.cpp" line="628"/>
        <source>There are no partitions to install on.</source>
        <translation>Няма дялове, върху които да се извърши инсталирането.</translation>
    </message>
</context>
<context>
    <name>PlasmaLnfJob</name>
    <message>
        <location filename="../src/modules/plasmalnf/PlasmaLnfJob.cpp" line="33"/>
        <source>Plasma Look-and-Feel Job</source>
        <translation>Оформление и външен вид в стил Plasma</translation>
    </message>
    <message>
        <location filename="../src/modules/plasmalnf/PlasmaLnfJob.cpp" line="57"/>
        <location filename="../src/modules/plasmalnf/PlasmaLnfJob.cpp" line="58"/>
        <source>Could not select KDE Plasma Look-and-Feel package</source>
        <translation>Неуспех при избиране на пакет с оформление на външен вид на Plasma KDE</translation>
    </message>
</context>
<context>
    <name>PlasmaLnfPage</name>
    <message>
        <location filename="../src/modules/plasmalnf/page_plasmalnf.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/plasmalnf/PlasmaLnfPage.cpp" line="79"/>
        <source>Please choose a look-and-feel for the KDE Plasma Desktop. You can also skip this step and configure the look-and-feel once the system is set up. Clicking on a look-and-feel selection will give you a live preview of that look-and-feel.</source>
        <translation>Моля, изберете оформление и външен вид на работния плот на KDE Plasma. Можете също да пропуснете тази стъпка и да я  конфигурирате, след като системата е настроена. Щракването върху това поле ще ви даде предварителен изглед на това оформление.</translation>
    </message>
    <message>
        <location filename="../src/modules/plasmalnf/PlasmaLnfPage.cpp" line="84"/>
        <source>Please choose a look-and-feel for the KDE Plasma Desktop. You can also skip this step and configure the look-and-feel once the system is installed. Clicking on a look-and-feel selection will give you a live preview of that look-and-feel.</source>
        <translation>Моля, изберете оформление и външен вид на работния плот на KDE Plasma. Можете също да пропуснете тази стъпка и да я  конфигурирате, след като системата е инсталирана. Щракването върху това поле ще ви даде предварителен изглед на това оформление.</translation>
    </message>
</context>
<context>
    <name>PlasmaLnfViewStep</name>
    <message>
        <location filename="../src/modules/plasmalnf/PlasmaLnfViewStep.cpp" line="43"/>
        <source>Look-and-Feel</source>
        <translation>Външен вид</translation>
    </message>
</context>
<context>
    <name>PreserveFiles</name>
    <message>
        <location filename="../src/modules/preservefiles/PreserveFiles.cpp" line="79"/>
        <source>Saving files for later ...</source>
        <translation>Запазване на файловете за по -късно...</translation>
    </message>
    <message>
        <location filename="../src/modules/preservefiles/PreserveFiles.cpp" line="118"/>
        <source>No files configured to save for later.</source>
        <translation>Няма конфигурирани файлове за запазване за по -късно.</translation>
    </message>
    <message>
        <location filename="../src/modules/preservefiles/PreserveFiles.cpp" line="172"/>
        <source>Not all of the configured files could be preserved.</source>
        <translation>Не всички конфигурирани файлове могат да бъдат запазени.</translation>
    </message>
</context>
<context>
    <name>ProcessResult</name>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="412"/>
        <source>
There was no output from the command.</source>
        <translation>
Няма резултат от командата.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="413"/>
        <source>
Output:
</source>
        <translation>
Резултат:
</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="417"/>
        <source>External command crashed.</source>
        <translation>Външната команда се срина.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="418"/>
        <source>Command &lt;i&gt;%1&lt;/i&gt; crashed.</source>
        <translation>Командата &lt;i&gt;%1 &lt;/i&gt;  се срина.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="423"/>
        <source>External command failed to start.</source>
        <translation>Външната команда не успя да се стартира.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="424"/>
        <source>Command &lt;i&gt;%1&lt;/i&gt; failed to start.</source>
        <translation>Команда &lt;i&gt;%1 &lt;/i&gt; не успя да се стартира.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="428"/>
        <source>Internal error when starting command.</source>
        <translation>Вътрешна грешка при стартиране на команда.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="429"/>
        <source>Bad parameters for process job call.</source>
        <translation>Невалидни параметри за извикване на задача за процес.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="433"/>
        <source>External command failed to finish.</source>
        <translation>Външната команда не успя да завърши.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="434"/>
        <source>Command &lt;i&gt;%1&lt;/i&gt; failed to finish in %2 seconds.</source>
        <translation>Командата &lt;i&gt; %1 &lt;/i&gt; не успя да завърши за %2 секунди.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="441"/>
        <source>External command finished with errors.</source>
        <translation>Външната команда завърши с грешки.</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/utils/CalamaresUtilsSystem.cpp" line="442"/>
        <source>Command &lt;i&gt;%1&lt;/i&gt; finished with exit code %2.</source>
        <translation>Командата &lt;i&gt; %1 &lt;/i&gt; завърши с изходен код %2.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/libcalamares/locale/Label.cpp" line="29"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/partition/FileSystem.cpp" line="28"/>
        <source>unknown</source>
        <translation>неизвестна</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/partition/FileSystem.cpp" line="30"/>
        <source>extended</source>
        <translation>разширена</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/partition/FileSystem.cpp" line="32"/>
        <source>unformatted</source>
        <translation>неформатирана</translation>
    </message>
    <message>
        <location filename="../src/libcalamares/partition/FileSystem.cpp" line="34"/>
        <source>swap</source>
        <translation>swap</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/keyboardwidget/keyboardglobal.cpp" line="130"/>
        <location filename="../src/modules/keyboard/keyboardwidget/keyboardglobal.cpp" line="167"/>
        <source>Default</source>
        <translation>По подразбиране</translation>
    </message>
    <message>
        <location filename="../src/modules/machineid/Workers.cpp" line="64"/>
        <location filename="../src/modules/machineid/Workers.cpp" line="72"/>
        <location filename="../src/modules/machineid/Workers.cpp" line="76"/>
        <location filename="../src/modules/machineid/Workers.cpp" line="93"/>
        <source>File not found</source>
        <translation>Файлът не е намерен</translation>
    </message>
    <message>
        <location filename="../src/modules/machineid/Workers.cpp" line="65"/>
        <source>Path &lt;pre&gt;%1&lt;/pre&gt; must be an absolute path.</source>
        <translation>Пътят &lt;pre&gt;%1 &lt;/pre&gt; трябва да бъде абсолютен път.</translation>
    </message>
    <message>
        <location filename="../src/modules/machineid/MachineIdJob.cpp" line="83"/>
        <source>Directory not found</source>
        <translation>Директорията не е намерена</translation>
    </message>
    <message>
        <location filename="../src/modules/machineid/MachineIdJob.cpp" line="84"/>
        <location filename="../src/modules/machineid/Workers.cpp" line="94"/>
        <source>Could not create new random file &lt;pre&gt;%1&lt;/pre&gt;.</source>
        <translation>Неуспех при създаването на нов случаен файл &lt;pre&gt;%1 &lt;/pre&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/PackageModel.cpp" line="70"/>
        <source>No product</source>
        <translation>Няма продукт</translation>
    </message>
    <message>
        <location filename="../src/modules/packagechooser/PackageModel.cpp" line="78"/>
        <source>No description provided.</source>
        <translation>Не е предоставено описание.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionDialogHelpers.cpp" line="40"/>
        <source>(no mount point)</source>
        <translation>(без точка на монтиране)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/PartitionLabelsView.cpp" line="41"/>
        <source>Unpartitioned space or unknown partition table</source>
        <translation>Неразделено пространство или неизвестна таблица на дяловете</translation>
    </message>
</context>
<context>
    <name>Recommended</name>
    <message>
        <location filename="../src/modules/welcomeq/Recommended.qml" line="40"/>
        <source>&lt;p&gt;This computer does not satisfy some of the recommended requirements for setting up %1.&lt;br/&gt;
        Setup can continue, but some features might be disabled.&lt;/p&gt;</source>
        <translation>&lt;p&gt; Този компютър не удовлетворява някои от препоръчителните изисквания за настройване на %1. &lt;br/&gt; 
        Инсталирането може да продължи, но някои функции могат да бъдат деактивирани.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>RemoveUserJob</name>
    <message>
        <location filename="../src/modules/removeuser/RemoveUserJob.cpp" line="34"/>
        <source>Remove live user from target system</source>
        <translation>Премахване на потребителите от инсталационната система</translation>
    </message>
</context>
<context>
    <name>RemoveVolumeGroupJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/RemoveVolumeGroupJob.cpp" line="24"/>
        <location filename="../src/modules/partition/jobs/RemoveVolumeGroupJob.cpp" line="36"/>
        <source>Remove Volume Group named %1.</source>
        <translation>Премахване на група дялове %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/RemoveVolumeGroupJob.cpp" line="30"/>
        <source>Remove Volume Group named &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Премахване на група дялове &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/RemoveVolumeGroupJob.cpp" line="48"/>
        <source>The installer failed to remove a volume group named &apos;%1&apos;.</source>
        <translation>Инсталаторът не успя да премахне група дялове с име &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>ReplaceWidget</name>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="127"/>
        <source>Select where to install %1.&lt;br/&gt;&lt;font color=&quot;red&quot;&gt;Warning: &lt;/font&gt;this will delete all files on the selected partition.</source>
        <translation>Изберете къде да инсталирате %1.&lt;br/&gt;&lt;font color=&quot;red&quot;&gt;Предупреждение: &lt;/font&gt;това ще изтрие всички файлове върху избраният дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="149"/>
        <source>The selected item does not appear to be a valid partition.</source>
        <translation>Избраният предмет не изглежда да е валиден дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="157"/>
        <source>%1 cannot be installed on empty space. Please select an existing partition.</source>
        <translation>%1 не може да бъде инсталиран на празно пространство. Моля изберете съществуващ дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="167"/>
        <source>%1 cannot be installed on an extended partition. Please select an existing primary or logical partition.</source>
        <translation>%1 не може да бъде инсталиран върху разширен дял. Моля изберете съществуващ основен или логически дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="177"/>
        <source>%1 cannot be installed on this partition.</source>
        <translation>%1 не може да бъде инсталиран върху този дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="183"/>
        <source>Data partition (%1)</source>
        <translation>Дял на данните (%1)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="203"/>
        <source>Unknown system partition (%1)</source>
        <translation>Непознат системен дял (%1)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="207"/>
        <source>%1 system partition (%2)</source>
        <translation>%1 системен дял (%2)</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="218"/>
        <source>&lt;strong&gt;%4&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;The partition %1 is too small for %2. Please select a partition with capacity at least %3 GiB.</source>
        <translation>&lt;strong&gt;%4&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;Дялът %1 е твърде малък за %2. Моля изберете дял с капацитет поне %3 ГБ.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="240"/>
        <source>&lt;strong&gt;%2&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;An EFI system partition cannot be found anywhere on this system. Please go back and use manual partitioning to set up %1.</source>
        <translation>&lt;strong&gt;%2&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;EFI системен дял не е намерен. Моля, опитайте пак като използвате ръчно поделяне за %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="251"/>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="267"/>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="292"/>
        <source>&lt;strong&gt;%3&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;%1 will be installed on %2.&lt;br/&gt;&lt;font color=&quot;red&quot;&gt;Warning: &lt;/font&gt;all data on partition %2 will be lost.</source>
        <translation>&lt;strong&gt;%3&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;%1 ще бъде инсталиран върху %2.&lt;br/&gt;&lt;font color=&quot;red&quot;&gt;Предупреждение: &lt;/font&gt;всички данни на дял %2 ще бъдат изгубени.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="259"/>
        <source>The EFI system partition at %1 will be used for starting %2.</source>
        <translation>EFI системен дял в %1 ще бъде използван за стартиране на %2.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ReplaceWidget.cpp" line="275"/>
        <source>EFI system partition:</source>
        <translation>EFI системен дял:</translation>
    </message>
</context>
<context>
    <name>Requirements</name>
    <message>
        <location filename="../src/modules/welcomeq/Requirements.qml" line="38"/>
        <source>&lt;p&gt;This computer does not satisfy the minimum requirements for installing %1.&lt;br/&gt;
        Installation cannot continue.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Този компютър не отговаря на минималните изисквания за инсталирането на %1.&lt;br/&gt; 
Инсталацията не може да продължи.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/Requirements.qml" line="40"/>
        <source>&lt;p&gt;This computer does not satisfy some of the recommended requirements for setting up %1.&lt;br/&gt;
        Setup can continue, but some features might be disabled.&lt;/p&gt;</source>
        <translation>&lt;p&gt; Този компютър не удовлетворява някои от препоръчителните изисквания за настройване на %1. &lt;br/&gt; 
        Инсталирането може да продължи, но някои функции могат да бъдат деактивирани.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ResizeFSJob</name>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="46"/>
        <source>Resize Filesystem Job</source>
        <translation>Оразмеряване на файловата система</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="169"/>
        <source>Invalid configuration</source>
        <translation>Невалидна конфигурация</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="170"/>
        <source>The file-system resize job has an invalid configuration and will not run.</source>
        <translation>Работата за преоразмеряване на файловата система има невалидна конфигурация и няма да се изпълни.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="175"/>
        <source>KPMCore not Available</source>
        <translation>KPMCore не е наличен</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="176"/>
        <source>Calamares cannot start KPMCore for the file-system resize job.</source>
        <translation>Calamares не може да започне KPMCore за преоразмеряването на файловата система.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="184"/>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="193"/>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="204"/>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="213"/>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="231"/>
        <source>Resize Failed</source>
        <translation>Преоразмеряването е неуспешно</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="186"/>
        <source>The filesystem %1 could not be found in this system, and cannot be resized.</source>
        <translation>Файловата система %1 не може да бъде намерена на този диск и не може да бъде преоразмерена.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="187"/>
        <source>The device %1 could not be found in this system, and cannot be resized.</source>
        <translation>Устройството %1 не може да бъде намерено в тази система и не може да бъде преоразмерено.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="195"/>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="206"/>
        <source>The filesystem %1 cannot be resized.</source>
        <translation>Файловата система %1 не може да бъде преоразмерена.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="196"/>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="207"/>
        <source>The device %1 cannot be resized.</source>
        <translation>Устройството %1 не може да бъде преоразмерено.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="214"/>
        <source>The filesystem %1 must be resized, but cannot.</source>
        <translation>Файловата система %1 трябва да бъде преоразмерена, но действието не може се извърши.</translation>
    </message>
    <message>
        <location filename="../src/modules/fsresizer/ResizeFSJob.cpp" line="215"/>
        <source>The device %1 must be resized, but cannot</source>
        <translation>Устройството %1 трябва да бъде преоразмерено,  но действието не може се извърши</translation>
    </message>
</context>
<context>
    <name>ResizePartitionJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/ResizePartitionJob.cpp" line="40"/>
        <source>Resize partition %1.</source>
        <translation>Преоразмеряване на дял %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ResizePartitionJob.cpp" line="47"/>
        <source>Resize &lt;strong&gt;%2MiB&lt;/strong&gt; partition &lt;strong&gt;%1&lt;/strong&gt; to &lt;strong&gt;%3MiB&lt;/strong&gt;.</source>
        <translation>Преоразмеряване на &lt;strong&gt;%2MiB &lt;/strong&gt; дял &lt;strong&gt;%1 &lt;/strong&gt; до&lt;strong&gt;%3MiB &lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ResizePartitionJob.cpp" line="58"/>
        <source>Resizing %2MiB partition %1 to %3MiB.</source>
        <translation>Преоразмеряване на %2MiB дял %1 до %3MiB.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ResizePartitionJob.cpp" line="77"/>
        <source>The installer failed to resize partition %1 on disk &apos;%2&apos;.</source>
        <translation>Инсталаторът не успя да преоразмери дял %1 върху диск &apos;%2&apos;.</translation>
    </message>
</context>
<context>
    <name>ResizeVolumeGroupDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/ResizeVolumeGroupDialog.cpp" line="30"/>
        <source>Resize Volume Group</source>
        <translation>Преоразмеряване на група дялове</translation>
    </message>
</context>
<context>
    <name>ResizeVolumeGroupJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/ResizeVolumeGroupJob.cpp" line="27"/>
        <location filename="../src/modules/partition/jobs/ResizeVolumeGroupJob.cpp" line="45"/>
        <source>Resize volume group named %1 from %2 to %3.</source>
        <translation>Преоразмеряване на група дялове %1 от %2 на %3.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ResizeVolumeGroupJob.cpp" line="36"/>
        <source>Resize volume group named &lt;strong&gt;%1&lt;/strong&gt; from &lt;strong&gt;%2&lt;/strong&gt; to &lt;strong&gt;%3&lt;/strong&gt;.</source>
        <translation>Преоразмеряване на група дялове &lt;strong&gt;%1&lt;/strong&gt; от&lt;strong&gt;%2&lt;/strong&gt; на&lt;strong&gt;%3&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/ResizeVolumeGroupJob.cpp" line="60"/>
        <source>The installer failed to resize a volume group named &apos;%1&apos;.</source>
        <translation>Инсталаторът не успя да преоразмери група дялове &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>ResultsListDialog</name>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="133"/>
        <source>For best results, please ensure that this computer:</source>
        <translation>За най-добри резултати, моля уверете се, че този компютър:</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="134"/>
        <source>System requirements</source>
        <translation>Системни изисквания</translation>
    </message>
</context>
<context>
    <name>ResultsListWidget</name>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="256"/>
        <source>This computer does not satisfy the minimum requirements for setting up %1.&lt;br/&gt;Setup cannot continue. &lt;a href=&quot;#details&quot;&gt;Details...&lt;/a&gt;</source>
        <translation>Този компютър не отговаря на минималните изисквания за настройване на %1.&lt;br/&gt; Настройката не може да продължи. &lt;a href=&quot;#details&quot;&gt; Подробности...&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="260"/>
        <source>This computer does not satisfy the minimum requirements for installing %1.&lt;br/&gt;Installation cannot continue. &lt;a href=&quot;#details&quot;&gt;Details...&lt;/a&gt;</source>
        <translation>Този компютър не отговаря на минималните изисквания за инсталиране %1.&lt;br/&gt;Инсталацията не може да продължи.
&lt;a href=&quot;#details&quot;&gt;Детайли...&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="267"/>
        <source>This computer does not satisfy some of the recommended requirements for setting up %1.&lt;br/&gt;Setup can continue, but some features might be disabled.</source>
        <translation>Този ​​компютър не удовлетворява някои от препоръчителните изисквания за настройването на %1. &lt;br/&gt; Настройката може да продължи, но някои функции могат да бъдат деактивирани.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="271"/>
        <source>This computer does not satisfy some of the recommended requirements for installing %1.&lt;br/&gt;Installation can continue, but some features might be disabled.</source>
        <translation>Този компютър не отговаря на някои от препоръчителните изисквания за инсталиране %1.&lt;br/&gt;Инсталацията може да продължи, но някои свойства могат да бъдат недостъпни.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/checker/ResultsListWidget.cpp" line="280"/>
        <source>This program will ask you some questions and set up %2 on your computer.</source>
        <translation>Тази програма ще ви зададе няколко въпроса и ще конфигурира %2 на вашия компютър.</translation>
    </message>
</context>
<context>
    <name>ScanningDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/ScanningDialog.cpp" line="64"/>
        <source>Scanning storage devices...</source>
        <translation>Сканиране на устройствата за съхранение</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/ScanningDialog.cpp" line="64"/>
        <source>Partitioning</source>
        <translation>Разделяне</translation>
    </message>
</context>
<context>
    <name>SetHostNameJob</name>
    <message>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="37"/>
        <source>Set hostname %1</source>
        <translation>Задаване на име на хоста %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="44"/>
        <source>Set hostname &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Задаване на име на хост &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="51"/>
        <source>Setting hostname %1.</source>
        <translation>Задаване на име на хост %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="122"/>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="129"/>
        <source>Internal Error</source>
        <translation>Вътрешна грешка</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="137"/>
        <location filename="../src/modules/users/SetHostNameJob.cpp" line="146"/>
        <source>Cannot write hostname to target system</source>
        <translation>Не може да се запише името на хоста на целевата система</translation>
    </message>
</context>
<context>
    <name>SetKeyboardLayoutJob</name>
    <message>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="55"/>
        <source>Set keyboard model to %1, layout to %2-%3</source>
        <translation>Задаване на модела на клавиатурата на %1, оформлението на %2-%3</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="368"/>
        <source>Failed to write keyboard configuration for the virtual console.</source>
        <translation>Неуспешно записването на клавиатурна конфигурация за виртуалната конзола.</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="369"/>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="397"/>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="414"/>
        <source>Failed to write to %1</source>
        <translation>Неуспешно записване върху %1</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="396"/>
        <source>Failed to write keyboard configuration for X11.</source>
        <translation>Неуспешно записване на клавиатурна конфигурация за X11.</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboard/SetKeyboardLayoutJob.cpp" line="413"/>
        <source>Failed to write keyboard configuration to existing /etc/default directory.</source>
        <translation>Неуспешно записване на клавиатурна конфигурация в съществуващата директория /etc/default.</translation>
    </message>
</context>
<context>
    <name>SetPartFlagsJob</name>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="43"/>
        <source>Set flags on partition %1.</source>
        <translation>Задаване на флагове на дял %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="49"/>
        <source>Set flags on %1MiB %2 partition.</source>
        <translation>Задаване на флагове на %1MiB %2 дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="53"/>
        <source>Set flags on new partition.</source>
        <translation>Задаване на флагове на нов дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="65"/>
        <source>Clear flags on partition &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Изчистване на флаговете на дял &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="71"/>
        <source>Clear flags on %1MiB &lt;strong&gt;%2&lt;/strong&gt; partition.</source>
        <translation>Изчистване на флагове на %1MiB &lt;strong&gt; %2 &lt;/strong&gt; дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="75"/>
        <source>Clear flags on new partition.</source>
        <translation>Изчистване на флагове на нов дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="80"/>
        <source>Flag partition &lt;strong&gt;%1&lt;/strong&gt; as &lt;strong&gt;%2&lt;/strong&gt;.</source>
        <translation>Задаване на флаг на дял &lt;strong&gt;%1&lt;/strong&gt; като &lt;strong&gt;%2&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="89"/>
        <source>Flag %1MiB &lt;strong&gt;%2&lt;/strong&gt; partition as &lt;strong&gt;%3&lt;/strong&gt;.</source>
        <translation>Задаване на флаг на %1MiB &lt;strong&gt;%2&lt;/strong&gt; дял като &lt;strong&gt;%3&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="96"/>
        <source>Flag new partition as &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Задаване на флаг на новия дял като &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="108"/>
        <source>Clearing flags on partition &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Изчистване на флаговете на дял &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="114"/>
        <source>Clearing flags on %1MiB &lt;strong&gt;%2&lt;/strong&gt; partition.</source>
        <translation>Изчистване на флагове на %1MiB &lt;strong&gt; %2 &lt;/strong&gt; дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="119"/>
        <source>Clearing flags on new partition.</source>
        <translation>Изчистване на флаговете на новия дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="124"/>
        <source>Setting flags &lt;strong&gt;%2&lt;/strong&gt; on partition &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>Задаване на флагове &lt;strong&gt;%2&lt;/strong&gt; на дял &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="133"/>
        <source>Setting flags &lt;strong&gt;%3&lt;/strong&gt; on %1MiB &lt;strong&gt;%2&lt;/strong&gt; partition.</source>
        <translation>Задаване на флагове &lt;strong&gt;%3 &lt;/strong&gt;  на %1MiB &lt;strong&gt;%2 &lt;/strong&gt; дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="140"/>
        <source>Setting flags &lt;strong&gt;%1&lt;/strong&gt; on new partition.</source>
        <translation>Задаване на флагове &lt;strong&gt;%1&lt;/strong&gt; на новия дял.</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/jobs/SetPartitionFlagsJob.cpp" line="156"/>
        <source>The installer failed to set flags on partition %1.</source>
        <translation>Инсталаторът не успя да зададе флагове на дял %1.</translation>
    </message>
</context>
<context>
    <name>SetPasswordJob</name>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="40"/>
        <source>Set password for user %1</source>
        <translation>Задаване на парола за потребител %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="47"/>
        <source>Setting password for user %1.</source>
        <translation>Задаване на парола за потребител %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="81"/>
        <source>Bad destination system path.</source>
        <translation>Лоша дестинация за системен път.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="82"/>
        <source>rootMountPoint is %1</source>
        <translation>rootMountPoint е %1</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="88"/>
        <source>Cannot disable root account.</source>
        <translation>Не може да деактивира root акаунтът.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="89"/>
        <source>passwd terminated with error code %1.</source>
        <translation>passwd прекъсна с код за грешка %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="97"/>
        <source>Cannot set password for user %1.</source>
        <translation>Не може да се зададе парола за потребител %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/SetPasswordJob.cpp" line="98"/>
        <source>usermod terminated with error code %1.</source>
        <translation>usermod прекъсна с грешка %1.</translation>
    </message>
</context>
<context>
    <name>SetTimezoneJob</name>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="34"/>
        <source>Set timezone to %1/%2</source>
        <translation>Задаване на часовата зона на %1/%2</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="62"/>
        <source>Cannot access selected timezone path.</source>
        <translation>Няма достъп до избрания път за часова зона.</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="63"/>
        <source>Bad path: %1</source>
        <translation>Невалиден път: %1</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="71"/>
        <source>Cannot set timezone.</source>
        <translation>Не може да се зададе часова зона.</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="72"/>
        <source>Link creation failed, target: %1; link name: %2</source>
        <translation>Неуспешно създаване на връзка: %1; име на връзка: %2</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="77"/>
        <source>Cannot set timezone,</source>
        <translation>Не може да се зададе часова зона,</translation>
    </message>
    <message>
        <location filename="../src/modules/locale/SetTimezoneJob.cpp" line="78"/>
        <source>Cannot open /etc/timezone for writing</source>
        <translation>Не може да се отвори /etc/timezone за записване</translation>
    </message>
</context>
<context>
    <name>SetupGroupsJob</name>
    <message>
        <location filename="../src/modules/users/MiscJobs.cpp" line="166"/>
        <source>Preparing groups.</source>
        <translation>Подготовка на групите.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/MiscJobs.cpp" line="178"/>
        <location filename="../src/modules/users/MiscJobs.cpp" line="183"/>
        <source>Could not create groups in target system</source>
        <translation>Неуспех при създаването на групи в целевата система</translation>
    </message>
    <message>
        <location filename="../src/modules/users/MiscJobs.cpp" line="184"/>
        <source>These groups are missing in the target system: %1</source>
        <translation>Тези групи липсват в целевата система: %1</translation>
    </message>
</context>
<context>
    <name>SetupSudoJob</name>
    <message>
        <location filename="../src/modules/users/MiscJobs.cpp" line="33"/>
        <source>Configure &lt;pre&gt;sudo&lt;/pre&gt; users.</source>
        <translation>Конфигуриране на &lt;pre&gt; sudo &lt;/pre&gt; потребители.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/MiscJobs.cpp" line="55"/>
        <source>Cannot chmod sudoers file.</source>
        <translation>Не може да се изпълни chmod върху файла sudoers.</translation>
    </message>
    <message>
        <location filename="../src/modules/users/MiscJobs.cpp" line="60"/>
        <source>Cannot create sudoers file for writing.</source>
        <translation>Не може да се създаде файл sudoers за записване.</translation>
    </message>
</context>
<context>
    <name>ShellProcessJob</name>
    <message>
        <location filename="../src/modules/shellprocess/ShellProcessJob.cpp" line="41"/>
        <source>Shell Processes Job</source>
        <translation>Процесни задачи на обвивката</translation>
    </message>
</context>
<context>
    <name>SlideCounter</name>
    <message>
        <location filename="../src/qml/calamares/slideshow/SlideCounter.qml" line="27"/>
        <source>%L1 / %L2</source>
        <extracomment>slide counter, %1 of %2 (numeric)</extracomment>
        <translation>%L1 / %L2</translation>
    </message>
</context>
<context>
    <name>SummaryPage</name>
    <message>
        <location filename="../src/modules/summary/SummaryPage.cpp" line="48"/>
        <source>This is an overview of what will happen once you start the setup procedure.</source>
        <translation>Това е преглед на това, което ще се случи, след като започнете процедурата за настройване.</translation>
    </message>
    <message>
        <location filename="../src/modules/summary/SummaryPage.cpp" line="50"/>
        <source>This is an overview of what will happen once you start the install procedure.</source>
        <translation>Това е преглед на промените, които ще се извършат, след като започнете процедурата по инсталиране.</translation>
    </message>
</context>
<context>
    <name>SummaryViewStep</name>
    <message>
        <location filename="../src/modules/summary/SummaryViewStep.cpp" line="36"/>
        <source>Summary</source>
        <translation>Обобщение</translation>
    </message>
</context>
<context>
    <name>TrackingInstallJob</name>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="37"/>
        <source>Installation feedback</source>
        <translation>Обратна връзка за инсталирането</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="43"/>
        <source>Sending installation feedback.</source>
        <translation>Изпращане на обратна връзка за инсталирането.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="60"/>
        <source>Internal error in install-tracking.</source>
        <translation>Вътрешна грешка при проследяване на инсталирането.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="61"/>
        <source>HTTP request timed out.</source>
        <translation>Времето на HTTP заявката изтече.</translation>
    </message>
</context>
<context>
    <name>TrackingKUserFeedbackJob</name>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="122"/>
        <source>KDE user feedback</source>
        <translation>Потребителска обратна връзка на KDE</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="128"/>
        <source>Configuring KDE user feedback.</source>
        <translation>Конфигуриране на потребителска обратна връзка на KDE.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="150"/>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="156"/>
        <source>Error in KDE user feedback configuration.</source>
        <translation>Грешка в конфигурацията за потребителска обратна връзка за KDE.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="151"/>
        <source>Could not configure KDE user feedback correctly, script error %1.</source>
        <translation>Неуспех при конфигурирането на потребителска обратна връзка на KDE, грешка в скрипта %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="157"/>
        <source>Could not configure KDE user feedback correctly, Calamares error %1.</source>
        <translation>Неуспех при конфигурирането на потребителска обратна връзка за KDE, грешка на Calamares %1.</translation>
    </message>
</context>
<context>
    <name>TrackingMachineUpdateManagerJob</name>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="71"/>
        <source>Machine feedback</source>
        <translation>Машинна обратна връзка</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="77"/>
        <source>Configuring machine feedback.</source>
        <translation>Конфигуриране на машинна обратна връзка.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="100"/>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="106"/>
        <source>Error in machine feedback configuration.</source>
        <translation>Грешка в конфигурацията на машинната обратна връзка.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="101"/>
        <source>Could not configure machine feedback correctly, script error %1.</source>
        <translation>Неуспешно конфигуриране на машинна обратна връзка, грешка в скрипта %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingJobs.cpp" line="107"/>
        <source>Could not configure machine feedback correctly, Calamares error %1.</source>
        <translation>Неуспех при конфигурирането на машинна обратна връзка, грешка на Calamares %1.</translation>
    </message>
</context>
<context>
    <name>TrackingPage</name>
    <message>
        <location filename="../src/modules/tracking/page_trackingstep.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/page_trackingstep.ui" line="28"/>
        <source>Placeholder</source>
        <translation>Заместител</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/page_trackingstep.ui" line="76"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click here to send &lt;span style=&quot; font-weight:600;&quot;&gt;no information at all&lt;/span&gt; about your installation.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Щракнете тук, за да изпратите &lt;span style = &quot; font-weight: 600; &quot;&gt;изключване на всякаква информация &lt;/span&gt; за вашата инсталация.&lt;/p&gt;&lt;/ody&gt;&lt;/html&gt; </translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/page_trackingstep.ui" line="275"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;placeholder&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Click here for more information about user feedback&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;placeholder&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt; Щракнете тук за повече информация за обратна връзка от потребителите &lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingPage.cpp" line="86"/>
        <source>Tracking helps %1 to see how often it is installed, what hardware it is installed on and which applications are used. To see what will be sent, please click the help icon next to each area.</source>
        <translation>Проследяването помага на %1 да види колко често се инсталира, на какъв хардуер се инсталиран и кои приложения се използват. За да видите, какво ще бъде изпратено,моля, щракнете върху иконата за помощ.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingPage.cpp" line="91"/>
        <source>By selecting this you will send information about your installation and hardware. This information will only be sent &lt;b&gt;once&lt;/b&gt; after the installation finishes.</source>
        <translation>Като изберете това, ще изпратите информация за вашата инсталация и хардуер. Тази информация ще бъде изпратена  само &lt;b&gt; веднъж &lt;/b&gt; след завършване на инсталацията.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingPage.cpp" line="94"/>
        <source>By selecting this you will periodically send information about your &lt;b&gt;machine&lt;/b&gt; installation, hardware and applications, to %1.</source>
        <translation>Като изберете това, периодично ще изпращате информация за вашата&lt;b&gt; машинна &lt;/b&gt; инсталация, хардуер и приложения до %1.</translation>
    </message>
    <message>
        <location filename="../src/modules/tracking/TrackingPage.cpp" line="98"/>
        <source>By selecting this you will regularly send information about your &lt;b&gt;user&lt;/b&gt; installation, hardware, applications and application usage patterns, to %1.</source>
        <translation>Като изберете това, периодично ще изпращате информация за вашата &lt;b&gt; потребителска&lt;/b&gt; инсталация, хардуер, приложения и модели на  използване на приложенията до %1.</translation>
    </message>
</context>
<context>
    <name>TrackingViewStep</name>
    <message>
        <location filename="../src/modules/tracking/TrackingViewStep.cpp" line="49"/>
        <source>Feedback</source>
        <translation>Обратна връзка</translation>
    </message>
</context>
<context>
    <name>UsersPage</name>
    <message>
        <location filename="../src/modules/users/UsersPage.cpp" line="156"/>
        <source>&lt;small&gt;If more than one person will use this computer, you can create multiple accounts after setup.&lt;/small&gt;</source>
        <translation>&lt;small&gt;Ако повече от един човек ще използва този компютър, можете да създадете множество акаунти след настройването.&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/users/UsersPage.cpp" line="162"/>
        <source>&lt;small&gt;If more than one person will use this computer, you can create multiple accounts after installation.&lt;/small&gt;</source>
        <translation>&lt;small&gt;Ако повече от един човек ще използва този компютър, можете да създадете множество акаунти след инсталирането.&lt;/small&gt;</translation>
    </message>
</context>
<context>
    <name>UsersQmlViewStep</name>
    <message>
        <location filename="../src/modules/usersq/UsersQmlViewStep.cpp" line="39"/>
        <source>Users</source>
        <translation>Потребители</translation>
    </message>
</context>
<context>
    <name>UsersViewStep</name>
    <message>
        <location filename="../src/modules/users/UsersViewStep.cpp" line="48"/>
        <source>Users</source>
        <translation>Потребители</translation>
    </message>
</context>
<context>
    <name>VariantModel</name>
    <message>
        <location filename="../src/calamares/VariantModel.cpp" line="232"/>
        <source>Key</source>
        <comment>Column header for key/value</comment>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../src/calamares/VariantModel.cpp" line="236"/>
        <source>Value</source>
        <comment>Column header for key/value</comment>
        <translation>Стойност</translation>
    </message>
</context>
<context>
    <name>VolumeGroupBaseDialog</name>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="18"/>
        <source>Create Volume Group</source>
        <translation>Създаване на група дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="24"/>
        <source>List of Physical Volumes</source>
        <translation>Списък на физическите дялове</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="34"/>
        <source>Volume Group Name:</source>
        <translation>Име на група на дял:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="47"/>
        <source>Volume Group Type:</source>
        <translation>Вид на група на дял:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="60"/>
        <source>Physical Extent Size:</source>
        <translation>Размер на физически диапазон:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="70"/>
        <source> MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="86"/>
        <source>Total Size:</source>
        <translation>Общ размер:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="106"/>
        <source>Used Size:</source>
        <translation>Използван размер:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="126"/>
        <source>Total Sectors:</source>
        <translation>Общо сектори:</translation>
    </message>
    <message>
        <location filename="../src/modules/partition/gui/VolumeGroupBaseDialog.ui" line="146"/>
        <source>Quantity of LVs:</source>
        <translation>Брой на логични дялове:</translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="18"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="79"/>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="98"/>
        <source>Select application and system language</source>
        <translation>Избиране на език за приложенията и системата</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="140"/>
        <source>&amp;About</source>
        <translation>&amp;Относно</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="150"/>
        <source>Open donations website</source>
        <translation>Отваряне на уеб страницата за дарения</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="153"/>
        <source>&amp;Donate</source>
        <translation>&amp;Дарение</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="163"/>
        <source>Open help and support website</source>
        <translation>Отваряне на уеб страницата за помощ и поддръжка</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="166"/>
        <source>&amp;Support</source>
        <translation>&amp;Поддръжка</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="176"/>
        <source>Open issues and bug-tracking website</source>
        <translation>Отваряне на уеб страницата за проблеми и проследяване на грешки</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="179"/>
        <source>&amp;Known issues</source>
        <translation>&amp;Съществуващи проблеми</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="189"/>
        <source>Open release notes website</source>
        <translation>Отваряне на уеб страницата за бележки за изданието</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.ui" line="192"/>
        <source>&amp;Release notes</source>
        <translation>&amp;Бележки по изданието</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="216"/>
        <source>&lt;h1&gt;Welcome to the Calamares setup program for %1.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; Добре дошли в програмата за настройване на Calamares за %1. &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="217"/>
        <source>&lt;h1&gt;Welcome to %1 setup.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt; Добре дошли в инсталатора на %1 &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="222"/>
        <source>&lt;h1&gt;Welcome to the Calamares installer for %1.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Добре дошли в инсталатора Calamares за %1.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="223"/>
        <source>&lt;h1&gt;Welcome to the %1 installer.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Добре дошли в инсталатора на %1.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="228"/>
        <source>%1 support</source>
        <translation>%1 поддръжка</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="235"/>
        <source>About %1 setup</source>
        <translation>Относно инсталирането на %1</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="235"/>
        <source>About %1 installer</source>
        <translation>Относно инсталатор %1</translation>
    </message>
    <message>
        <location filename="../src/modules/welcome/WelcomePage.cpp" line="238"/>
        <source>&lt;h1&gt;%1&lt;/h1&gt;&lt;br/&gt;&lt;strong&gt;%2&lt;br/&gt;for %3&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;Copyright 2014-2017 Teo Mrnjavac &amp;lt;teo@kde.org&amp;gt;&lt;br/&gt;Copyright 2017-2020 Adriaan de Groot &amp;lt;groot@kde.org&amp;gt;&lt;br/&gt;Thanks to &lt;a href=&quot;https://calamares.io/team/&quot;&gt;the Calamares team&lt;/a&gt; and the &lt;a href=&quot;https://www.transifex.com/calamares/calamares/&quot;&gt;Calamares translators team&lt;/a&gt;.&lt;br/&gt;&lt;br/&gt;&lt;a href=&quot;https://calamares.io/&quot;&gt;Calamares&lt;/a&gt; development is sponsored by &lt;br/&gt;&lt;a href=&quot;http://www.blue-systems.com/&quot;&gt;Blue Systems&lt;/a&gt; - Liberating Software.</source>
        <translation>&lt;h1&gt;%1&lt;/h1&gt;&lt;br/&gt;                        &lt;strong&gt;%2&lt;br/&gt;                        за %3&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;                        Copyright 2014-2017 Teo Mrnjavac &amp;lt;teo@kde.org&amp;gt;&lt;br/&gt;                        Copyright 2017-2020 Adriaan de Groot &amp;lt;groot@kde.org&amp;gt;&lt;br/&gt;                        Благодарности на  &lt;a href=&quot;https://calamares.io/team/&quot;&gt;екипа на Calamares &lt;/a&gt;                        и  на&lt;a href=&quot;https://www.transifex.com/calamares/calamares/&quot;                        преводачите&lt;/a&gt;.&lt;br/&gt;&lt;br/&gt;                        Разработката на &lt;a href=&quot;https://calamares.io/&quot;&gt;Calamares&lt;/a&gt;                        е спонсорирана от&lt;br/                        &lt;a href=&quot;http://www.blue-systems.com/&quot;&gt;Blue Systems&lt;/a&gt; -                        Liberating Software.</translation>
    </message>
</context>
<context>
    <name>WelcomeQmlViewStep</name>
    <message>
        <location filename="../src/modules/welcomeq/WelcomeQmlViewStep.cpp" line="41"/>
        <source>Welcome</source>
        <translation>Добре дошли</translation>
    </message>
</context>
<context>
    <name>WelcomeViewStep</name>
    <message>
        <location filename="../src/modules/welcome/WelcomeViewStep.cpp" line="48"/>
        <source>Welcome</source>
        <translation>Добре дошли</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../src/modules/welcomeq/about.qml" line="47"/>
        <source>&lt;h1&gt;%1&lt;/h1&gt;&lt;br/&gt;
                        &lt;strong&gt;%2&lt;br/&gt;
                        for %3&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;
                        Copyright 2014-2017 Teo Mrnjavac &amp;lt;teo@kde.org&amp;gt;&lt;br/&gt;
                        Copyright 2017-2020 Adriaan de Groot &amp;lt;groot@kde.org&amp;gt;&lt;br/&gt;
                        Thanks to &lt;a href=&apos;https://calamares.io/team/&apos;&gt;the Calamares team&lt;/a&gt;
                        and the &lt;a href=&apos;https://www.transifex.com/calamares/calamares/&apos;&gt;Calamares
                        translators team&lt;/a&gt;.&lt;br/&gt;&lt;br/&gt;
                        &lt;a href=&apos;https://calamares.io/&apos;&gt;Calamares&lt;/a&gt;
                        development is sponsored by &lt;br/&gt;
                        &lt;a href=&apos;http://www.blue-systems.com/&apos;&gt;Blue Systems&lt;/a&gt; -
                        Liberating Software.</source>
        <translation>&lt;h1&gt;%1&lt;/h1&gt;&lt;br/&gt;
                        &lt;strong&gt;%2&lt;br/&gt;
                        за %3&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;
                        Copyright 2014-2017 Teo Mrnjavac &amp;lt;teo@kde.org&amp;gt;&lt;br/&gt;
                        Copyright 2017-2020 Adriaan de Groot &amp;lt;groot@kde.org&amp;gt;&lt;br/&gt;
                        Благодарности на  &lt;a href=&apos;https://calamares.io/team/&apos;&gt;екипа на Calamares &lt;/a&gt;
                        и  на&lt;a href=&apos;https://www.transifex.com/calamares/calamares/&apos;&gt;
                        преводачите&lt;/a&gt;.&lt;br/&gt;&lt;br/&gt;
                        Разработката на &lt;a href=&apos;https://calamares.io/&apos;&gt;Calamares&lt;/a&gt;
                        е спонсорирана от&lt;br/&gt;
                        &lt;a href=&apos;http://www.blue-systems.com/&apos;&gt;Blue Systems&lt;/a&gt; -
                        Liberating Software.</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/about.qml" line="96"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
</context>
<context>
    <name>i18n</name>
    <message>
        <location filename="../src/modules/localeq/i18n.qml" line="46"/>
        <source>&lt;h1&gt;Languages&lt;/h1&gt; &lt;/br&gt;
                    The system locale setting affects the language and character set for some command line user interface elements. The current setting is &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>&lt;h1&gt;Езици&lt;/h1&gt;&lt;/br&gt;
                    Регионалните настройки на системата засягат езика и набора от символи за някои елементи на потребителския команден ред. Текущата настройка е &lt;strong&gt;%1&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/i18n.qml" line="106"/>
        <source>&lt;h1&gt;Locales&lt;/h1&gt; &lt;/br&gt;
                    The system locale setting affects the numbers and dates format. The current setting is &lt;strong&gt;%1&lt;/strong&gt;.</source>
        <translation>&lt;h1&gt;Регионални настройки&lt;/h1&gt;&lt;/br&gt; 
                  Регионалните настройки на системата определя формата на числата и датите. Текущата настройка е &lt;strong&gt;%1 &lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../src/modules/localeq/i18n.qml" line="158"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
</context>
<context>
    <name>keyboardq</name>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="45"/>
        <source>Keyboard Model</source>
        <translation>Модел на клавиатурата</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="377"/>
        <source>Layouts</source>
        <translation>Подредби</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="148"/>
        <source>Keyboard Layout</source>
        <translation>Клавиатурна подредба</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="60"/>
        <source>Click your preferred keyboard model to select layout and variant, or use the default one based on the detected hardware.</source>
        <translation>Изберете предпочитания от вас модел на клавиатурата, подредба и вариант или използвайте настройките по подразбиране.</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="253"/>
        <source>Models</source>
        <translation>Модели</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="260"/>
        <source>Variants</source>
        <translation>Варианти</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="276"/>
        <source>Keyboard Variant</source>
        <translation>Варианти на клавиатурната подредба</translation>
    </message>
    <message>
        <location filename="../src/modules/keyboardq/keyboardq.qml" line="386"/>
        <source>Test your keyboard</source>
        <translation>Тествайте вашата клавиатура</translation>
    </message>
</context>
<context>
    <name>localeq</name>
    <message>
        <location filename="../src/modules/localeq/localeq.qml" line="81"/>
        <source>Change</source>
        <translation>Променяне</translation>
    </message>
</context>
<context>
    <name>notesqml</name>
    <message>
        <location filename="../src/modules/notesqml/notesqml.qml" line="50"/>
        <source>&lt;h3&gt;%1&lt;/h3&gt;
            &lt;p&gt;These are example release notes.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;%1 &lt;/h3&gt; 
            &lt;p&gt;Това са примерни бележки за издание.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>release_notes</name>
    <message>
        <location filename="../src/modules/welcomeq/release_notes.qml" line="45"/>
        <source>&lt;h3&gt;%1&lt;/h3&gt;
            &lt;p&gt;This an example QML file, showing options in RichText with Flickable content.&lt;/p&gt;

            &lt;p&gt;QML with RichText can use HTML tags, Flickable content is useful for touchscreens.&lt;/p&gt;

            &lt;p&gt;&lt;b&gt;This is bold text&lt;/b&gt;&lt;/p&gt;
            &lt;p&gt;&lt;i&gt;This is italic text&lt;/i&gt;&lt;/p&gt;
            &lt;p&gt;&lt;u&gt;This is underlined text&lt;/u&gt;&lt;/p&gt;
            &lt;p&gt;&lt;center&gt;This text will be center-aligned.&lt;/center&gt;&lt;/p&gt;
            &lt;p&gt;&lt;s&gt;This is strikethrough&lt;/s&gt;&lt;/p&gt;

            &lt;p&gt;Code example:
            &lt;code&gt;ls -l /home&lt;/code&gt;&lt;/p&gt;

            &lt;p&gt;&lt;b&gt;Lists:&lt;/b&gt;&lt;/p&gt;
            &lt;ul&gt;
                &lt;li&gt;Intel CPU systems&lt;/li&gt;
                &lt;li&gt;AMD CPU systems&lt;/li&gt;
            &lt;/ul&gt;

            &lt;p&gt;The vertical scrollbar is adjustable, current width set to 10.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;%1&lt;/h3&gt; 
&lt;p&gt;Това e примерен QML файл, показващ опции с форматиран текст и съдържание с бързо прелистване.&lt;/p&gt; 

&lt;p&gt; QML с форматиран текст може да използва HTML етикети, съдържание с бързо прелистване е удачно за сензорни екрани.&lt;/p&gt; 

           &lt;p&gt;&lt;b&gt; Това е текст с получер шрифт&lt;/b&gt;&lt;/p&gt; 
           &lt;p&gt;&lt;i&gt; Това е текст с курсивен шрифт&lt;/i&gt;&lt;/p&gt; 
           &lt;p&gt;&lt;u&gt; Това е подчертан текст &lt;/u&gt;&lt;/p&gt; 
           &lt;p&gt;&lt;center&gt; Този текст ще бъде подравнен в центъра.&lt;/center&gt;&lt;/p&gt; 
           &lt;p&gt;&lt;s&gt; Това е зачертаване &lt;/s&gt;&lt;/p&gt; 

           &lt;p&gt; Примерен код: 
&lt;code&gt; ls -l/home &lt;/code&gt;&lt;/p&gt; 

           &lt;p&gt;&lt;b&gt; Списъци:&lt;/b&gt;&lt;/p&gt; 
           &lt;ul&gt; 
                &lt;li&gt; Intel CPU системи &lt;/li&gt; 
                &lt;li&gt; AMD CPU системи &lt;/li&gt; 
           &lt;/ul&gt; 

           &lt;p&gt; Вертикалната лента за превъртане е регулируема, актуалната ширина е зададена на 10.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/release_notes.qml" line="76"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
</context>
<context>
    <name>usersq</name>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="36"/>
        <source>Pick your user name and credentials to login and perform admin tasks</source>
        <translation>Изберете потребителското си име и идентификационни данни, за да влезете  системата и изпълнявайте администраторски задачи</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="52"/>
        <source>What is your name?</source>
        <translation>Какво е вашето име?</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="59"/>
        <source>Your Full Name</source>
        <translation>Вашето пълно име</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="80"/>
        <source>What name do you want to use to log in?</source>
        <translation>Какво име искате да използвате за влизане?</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="87"/>
        <source>Login Name</source>
        <translation>Име за вход</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="103"/>
        <source>If more than one person will use this computer, you can create multiple accounts after installation.</source>
        <translation>Ако повече от един човек ще използва този компютър, можете да създадете множество акаунти след инсталирането.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="118"/>
        <source>What is the name of this computer?</source>
        <translation>Какво е името на този компютър?</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="125"/>
        <source>Computer Name</source>
        <translation>Име на компютър</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="140"/>
        <source>This name will be used if you make the computer visible to others on a network.</source>
        <translation>Това име ще бъде използвано, ако направите компютъра видим за другите в мрежата.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="155"/>
        <source>Choose a password to keep your account safe.</source>
        <translation>Изберете парола за да държите вашият акаунт в безопасност.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="166"/>
        <source>Password</source>
        <translation>Парола</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="185"/>
        <source>Repeat Password</source>
        <translation>Повтаряне на паролата</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="204"/>
        <source>Enter the same password twice, so that it can be checked for typing errors. A good password will contain a mixture of letters, numbers and punctuation, should be at least eight characters long, and should be changed at regular intervals.</source>
        <translation>Въведете една и съща парола два пъти, така че да може да бъде проверена за грешки във въвеждането.Добрата парола съдържа комбинация от букви, цифри и пунктуации. Трябва да е дълга поне осем знака и трябва да се променя периодично.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="216"/>
        <source>Validate passwords quality</source>
        <translation>Проверка на качеството на паролите</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="226"/>
        <source>When this box is checked, password-strength checking is done and you will not be able to use a weak password.</source>
        <translation>Ако това поле е маркирано, се извършва проверка на силата на паролата и няма да можете да използвате слаба парола.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="234"/>
        <source>Log in automatically without asking for the password</source>
        <translation>Автоматично влизане без изискване за парола</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="243"/>
        <source>Reuse user password as root password</source>
        <translation>Използване на потребителската парола и за парола на root</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="253"/>
        <source>Use the same password for the administrator account.</source>
        <translation>Използвайте същата парола за администраторския акаунт.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="268"/>
        <source>Choose a root password to keep your account safe.</source>
        <translation>Изберете парола за root, за да запазите акаунта си сигурен.</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="279"/>
        <source>Root Password</source>
        <translation>Парола за root</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="298"/>
        <source>Repeat Root Password</source>
        <translation>Повторете паролата за root</translation>
    </message>
    <message>
        <location filename="../src/modules/usersq/usersq.qml" line="318"/>
        <source>Enter the same password twice, so that it can be checked for typing errors.</source>
        <translation>Въведете една и съща парола два пъти, така че да може да бъде проверена за грешки във въвеждането.</translation>
    </message>
</context>
<context>
    <name>welcomeq</name>
    <message>
        <location filename="../src/modules/welcomeq/welcomeq.qml" line="35"/>
        <source>&lt;h3&gt;Welcome to the %1 &lt;quote&gt;%2&lt;/quote&gt; installer&lt;/h3&gt;
            &lt;p&gt;This program will ask you some questions and set up %1 on your computer.&lt;/p&gt;</source>
        <translation>&lt;h3&gt; Добре дошли в &lt;quote&gt; %2 &lt;/quote&gt; инсталатор на %1&lt;/h3&gt; 
&lt;p&gt; Тази програма ще ви зададе някои въпроси и ще инсталира %1 на вашия компютър.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/welcomeq.qml" line="66"/>
        <source>About</source>
        <translation>Относно</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/welcomeq.qml" line="80"/>
        <source>Support</source>
        <translation>Поддръжка</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/welcomeq.qml" line="91"/>
        <source>Known issues</source>
        <translation>Известни проблеми</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/welcomeq.qml" line="102"/>
        <source>Release notes</source>
        <translation>Бележки към изданието</translation>
    </message>
    <message>
        <location filename="../src/modules/welcomeq/welcomeq.qml" line="114"/>
        <source>Donate</source>
        <translation>Дарение</translation>
    </message>
</context>
</TS>
